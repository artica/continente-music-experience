

# INSTALL

* Install python2.7
* Install node.js
* Install node.js midi dependency
* Install Google Chrome Browser

To use Arduino instruments the local network must be configured following the IPs listed on fixed_ips.txt


# LAUNCH

Use the launch scripts in \launch\ to start localhost server, localhost websockets server, and launch the application in fullscreen mode.
App is optimized for FullHD resolutions, so your desktop must be at 1920x1080 or things will look wrong.


# EDITOR

Anything commented with the keyword EDITOR is used only for editing the tracks during game screen. Might have scroll bugs.
