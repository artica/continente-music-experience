#include <SPI.h>
#include <Ethernet.h>
#include <sha1.h>  
#include <WebSocketClient.h>

#define ID_INSTRUMENT 0
#define THRESHOLD_INSTRUMENT0 250
#define THRESHOLD_INSTRUMENT1 250
#define THRESHOLD_INSTRUMENT2 250
#define THRESHOLD_INSTRUMENT3 250

#define WEB_SOCKET_SERVER "192.168.1.130"
#define WEB_SOCKET_PORT 3001

#define DEBUG FALSE

byte mac[] = { 0x90, 0xa2, 0xda, 0x0f, 0x05, 0x7d };
byte ip[] = { 192, 168, 1, 122 };

const int sensor0_pin = A0;
const int sensor1_pin = A1;
const int sensor2_pin = A2;
const int sensor3_pin = A3;

int sensor0_value, sensor1_value, sensor2_value, sensor3_value;

static void hang()
{
	delay(1000);
	setup_WebSocket();
}

EthernetClient client;

class MyWebSocket : public websocket::WebSocket
{
public:
	MyWebSocket(Client &client) : websocket::WebSocket(client)
	{}

protected:
	virtual void onClose()
	{

#ifdef DEBUG
		Serial.println("WebSocket Closed !");
#endif

		hang();
	}

	virtual void onError(websocket::Result error)
	{

#ifdef DEBUG
		Serial.print("WebSocket error : "); Serial.println(error);
#endif
	}

	virtual void onTextFrame(char const* msg, uint16_t size, bool isLast)
	{
#ifdef DEBUG
		Serial.print("Got text frame : '"); Serial.print(msg); Serial.println("'");
#endif
	}
};

MyWebSocket webSocketClient(client);

void setup()
{

#ifdef DEBUG
	Serial.begin(115200);
#endif

	setup_WebSocket();

}

void setup_WebSocket()
{
        Serial.println(ip[3]);
	Ethernet.begin(mac, ip);

#ifdef DEBUG
	Serial.println(Ethernet.localIP());
#endif

	client.stop();

	if (client.connect(WEB_SOCKET_SERVER, WEB_SOCKET_PORT))
	{
#ifdef DEBUG
		Serial.println("Connected to : ");
		Serial.println(Ethernet.localIP());
#endif
	}
	else
	{
#ifdef DEBUG
		Serial.println("Connection Failed !");
#endif

		hang();	// hang on failure
	}

	if (websocket::clientHandshake(client, WEB_SOCKET_SERVER, "/ws") == websocket::Success_Ok)
	{
#ifdef DEBUG
		Serial.println("Hanshake Successful !");
#endif
	}
	else
	{
#ifdef DEBUG
		Serial.println("Handshake Failed !");
#endif

		hang();	// hang on failure
	}
	client.flush();
}

void loop()
{
	if (client.connected())
	{
		webSocketClient.dispatchEvents();

		sensor0_value = analogRead(sensor0_pin);
		sensor1_value = analogRead(sensor1_pin);
		sensor2_value = analogRead(sensor2_pin);
		sensor3_value = analogRead(sensor3_pin);

		if (sensor0_value > THRESHOLD_INSTRUMENT0 || sensor1_value > THRESHOLD_INSTRUMENT1 || sensor2_value > THRESHOLD_INSTRUMENT2 || sensor3_value > THRESHOLD_INSTRUMENT3)
		{
				String data = String();

				data = String(ID_INSTRUMENT) + "|" + String(sensor0_value > THRESHOLD_INSTRUMENT0) + "|" + String(sensor1_value > THRESHOLD_INSTRUMENT1) + "|"
					+ String(sensor2_value > THRESHOLD_INSTRUMENT2) + "|" + String(sensor3_value > THRESHOLD_INSTRUMENT3);

				webSocketClient.sendData(data.c_str());

				client.flush();
		}


	}
	else
	{
#ifdef DEBUG
		Serial.println("Connection Closed !");
#endif

		hang();
	}
}
