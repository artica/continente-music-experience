#include <Bounce.h>
#include <SPI.h>
#include <Ethernet.h>
#include <WebSocketClient.h>

#define DEGUB TRUE

#define SERIAL_BAUDRATE 9600

#define ID_INSTRUMENT 2
#define THRESHOLD_INSTRUMENT 1000
#define NUMBER_OF_NOTES 4

#define WEB_SOCKET_SERVER "192.168.1.130"    
#define WEB_SOCKET_PORT 3001

#define PIN0 2
#define PIN1 3
#define PIN2 4
#define PIN3 5
#define ACTIVE_STATE 0

#define DEBOUNCE_TIME 100
#define RETRIGGERING_TIME 1000

class MyWebSocket : public websocket::WebSocket
{
public:
	MyWebSocket(Client &client) : websocket::WebSocket(client)
	{}

protected:
	virtual void onClose()
	{

#ifdef DEBUG
		Serial.println("WebSocket Closed !");
#endif  
		hang();
	}

	virtual void onError(websocket::Result error)
	{

#ifdef DEBUG
		Serial.print("WebSocket error : "); Serial.println(error);
#endif
	}

	virtual void onTextFrame(char const* msg, uint16_t size, bool isLast)
	{
#ifdef DEBUG
		Serial.print("Got text frame : '"); Serial.print(msg); Serial.println("'");
#endif
	}
};

byte mac[] = { 0xde, 0xad, 0xbe, 0xef, 0xf0, 0x0e };
byte ip[] = { 192, 168, 1, 121 };

Bounce pinDebouncer[] = { Bounce(), Bounce(), Bounce(), Bounce() };

EthernetClient client;

unsigned long lastTime;

MyWebSocket webSocketClient(client);

int pinState[NUMBER_OF_NOTES] = { 0 };
unsigned long buttonPressTimestamp[NUMBER_OF_NOTES];

void setup()
{

#ifdef DEBUG
	Serial.begin(SERIAL_BAUDRATE);
#endif
	Serial.begin(9600);
	Serial.println("MAIN SETUP");

	pinMode(PIN0, INPUT_PULLUP);
	pinMode(PIN1, INPUT_PULLUP);
	pinMode(PIN2, INPUT_PULLUP);
	pinMode(PIN3, INPUT_PULLUP);

	for (int i = 0; i < NUMBER_OF_NOTES; i++)
		pinDebouncer[i] = Bounce();

	pinDebouncer[0].attach(PIN0);
	pinDebouncer[1].attach(PIN1);
	pinDebouncer[2].attach(PIN2);
	pinDebouncer[3].attach(PIN3);

	for (int i = 0; i < NUMBER_OF_NOTES; i++)
		pinDebouncer[i].interval(DEBOUNCE_TIME);

	//setup_WebSocket();

}

void loop()
{

	bool isDataToSend = false;

	bool pinChanged[NUMBER_OF_NOTES];

	bool notePlayed = false;
	bool noteStillPlaying = false;

	bool alreadyNote = false;

	for (int i = 0; i < NUMBER_OF_NOTES; i++)
		pinChanged[i] = pinDebouncer[i].update();

	String data = String();
	data = String(ID_INSTRUMENT);
	data += '|';

	//	if (client.connected())
	//{
	//webSocketClient.dispatchEvents();

	for (int i = 0; i < NUMBER_OF_NOTES; i++) {
		if (pinChanged[i])
		{
			notePlayed = true;
		}
	}
	if (notePlayed)
	{
		for (int i = 0; i < NUMBER_OF_NOTES; i++) 
			pinChanged[i] = 0;
		pinState[0] = 1;
		if (digitalRead(PIN0) == ACTIVE_STATE && digitalRead(PIN1) != ACTIVE_STATE)
			data += "1|";
		else
			data += "0|";
		if (digitalRead(PIN0) == ACTIVE_STATE && digitalRead(PIN1) == ACTIVE_STATE)
			data += "1|";
		else
			data += "0|";
		if (digitalRead(PIN1) == ACTIVE_STATE && digitalRead(PIN2) == ACTIVE_STATE)
			data += "1|";
		else
			data += "0|";
		if (digitalRead(PIN2) == ACTIVE_STATE && digitalRead(PIN3) == ACTIVE_STATE)
			data += '1';
		else
			data += '0';

		String aux = String(ID_INSTRUMENT);
		aux += "|0|0|0|0";
		if (data != aux && (millis() - lastTime) >= 100)
		{
			Serial.print("AQUI : ");
			Serial.println(data);

			//webSocketClient.sendData(data.c_str());
			//client.flush();
		}
	}
	else
		pinState[0] = 0;

	for (int i = 0; i < NUMBER_OF_NOTES; i++)
	{
		if (pinState[i] == 1 && (millis() - buttonPressTimestamp[i] >= RETRIGGERING_TIME))
		{
			buttonPressTimestamp[i] = millis();
			noteStillPlaying = true;
		}
	}

	if (noteStillPlaying && !notePlayed)
	{
		if (digitalRead(PIN0) == ACTIVE_STATE  /*digitalRead(PIN1) != ACTIVE_STATE*/)
			data += "1|";
		else
			data += "0|";
		if (digitalRead(PIN0) == ACTIVE_STATE && digitalRead(PIN1) == ACTIVE_STATE)
			data += "1|";
		else
			data += "0|";
		if (digitalRead(PIN1) == ACTIVE_STATE && digitalRead(PIN2) == ACTIVE_STATE)
			data += "1|";
		else
			data += "0|";
		if (digitalRead(PIN2) == ACTIVE_STATE && digitalRead(PIN3) == ACTIVE_STATE)
			data += '1';
		else
			data += '0';

		String aux = String(ID_INSTRUMENT);
		aux += "|0|0|0|0";
		if (data != aux && (millis() - lastTime) >= 100)
		{
			Serial.print("OU AQUI : ");
			Serial.println(data);

			//webSocketClient.sendData(data.c_str());
			//client.flush();
		}
	}

	notePlayed = false;
	noteStillPlaying = false;
}

