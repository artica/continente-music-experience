#include <SPI.h>
#include <Ethernet.h>
#include <sha1.h>  
#include <WebSocketClient.h>

#define ID_INSTRUMENT 0

#define WEB_SOCKET_SERVER "192.168.1.130"
#define WEB_SOCKET_PORT 3001

#define DEBUG TRUE

class MyWebSocket : public websocket::WebSocket
{
  public:
	MyWebSocket(Client &client) : websocket::WebSocket(client)
	{}

  protected:
	virtual void onClose()
	{

  #ifdef DEBUG
          Serial.println("WebSocket Closed !");
  #endif  
          hang();
	}

	virtual void onError(websocket::Result error)
	{

  #ifdef DEBUG
	  Serial.print("WebSocket error : "); Serial.println(error);
  #endif
	}

	virtual void onTextFrame(char const* msg, uint16_t size, bool isLast)
	{
  #ifdef DEBUG
	  Serial.print("Got text frame : '"); Serial.print(msg); Serial.println("'");
  #endif
	}
};



byte mac[] = { 0x90, 0xa2, 0xda, 0x0e, 0x0f, 0x66 };
byte ip[] = { 192, 168, 1, 122 };

int timeThreshold[] ={50,150,50,50};
int sensorPins[] = {A0,A1,A2,A3};
long sensorPrevTime[4];
int sensorsThreshold[]={250,512,100,250};
int sensorValues[4];
int sensorPrevValues[4];
EthernetClient client;

MyWebSocket webSocketClient(client);

void setup()
{

#ifdef DEBUG
	Serial.begin(115200);
#endif

	setup_WebSocket();

}

void loop()
{
  bool isDataToSend = false;
	if (client.connected())
	{
                webSocketClient.dispatchEvents();
                boolean activeState = false; 
                String data = String();
                data = String(ID_INSTRUMENT);
                for(int count =0; count<4; count++)
                {
                  sensorValues[count] = analogRead(sensorPins[count]);
                  if (sensorValues[count]>sensorsThreshold[count])
                  {
                    if((sensorPrevValues[count]<sensorsThreshold[count]) && (sensorPrevTime[count]< millis()- timeThreshold[count]))
                    {
                      sensorPrevTime[count] = millis();
                      activeState = true;
                      data += "|1";
                      isDataToSend = true;
                    }
                    else 
                    {
                      data += "|0";
                    }
                  }
                  else 
                  {
                    data += "|0";
                  }
                  sensorPrevValues[count] = sensorValues[count];
                }
                if (isDataToSend)
                {
#ifdef DEBUG                  
                  Serial.println(data);
#endif
                delay(10);
                webSocketClient.sendData(data.c_str());
		client.flush();
                }
	}
	else
	{
#ifdef DEBUG
		Serial.println("Connection Closed !");
#endif
		hang();
	}
}
