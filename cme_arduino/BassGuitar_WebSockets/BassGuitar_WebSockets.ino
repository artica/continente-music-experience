#include <Bounce.h>
#include <WebSocketServer.h>
#include <Base64.h>
#include <SPI.h>
#include <Ethernet.h>
#include <sha1.h>  
#include <WebSocketClient.h>

#define DEBUG FALSE

#define ID_INSTRUMENT 2
#define THRESHOLD_INSTRUMENT 1000

#define WEB_SOCKET_SERVER "192.168.1.130"    
//#define WEB_SOCKET_SERVER "192.168.1.110"

#define WEB_SOCKET_PORT 3001

byte mac[] = { 0xde, 0xad, 0xbe, 0xef, 0xf0, 0x0e };
byte ip[] = {192, 168, 1, 121};

const int touch3_pin = A0;
const int touch2_pin = A1;
const int touch1_pin = A2;
const int touch0_pin = A3;
const int strum0_pin = 2;
const int strum1_pin = 3;

int touch0_value, touch1_value, touch2_value, touch3_value;

Bounce strum0_debouncer = Bounce();
Bounce strum1_debouncer = Bounce();

static void hang()
{
	delay(1000);
	setup_WebSocket();
}

EthernetClient client;

class MyWebSocket : public websocket::WebSocket
{
public:
	MyWebSocket(Client &client) : websocket::WebSocket(client)
	{}

protected:
	virtual void onClose()
	{

#ifdef DEBUG
		Serial.println("WebSocket Closed !");
#endif

		hang();
	}

	virtual void onError(websocket::Result error)
	{

#ifdef DEBUG
		Serial.print("WebSocket error : "); Serial.println(error);
#endif
	}

	virtual void onTextFrame(char const* msg, uint16_t size, bool isLast)
	{
#ifdef DEBUG
		Serial.print("Got text frame : '"); Serial.print(msg); Serial.println("'");
#endif
	}
};

MyWebSocket webSocketClient(client);

void setup()
{
	pinMode(strum0_pin, INPUT_PULLUP);
	pinMode(strum1_pin, INPUT_PULLUP);

	strum0_debouncer.attach(strum0_pin);
	strum0_debouncer.interval(50);

	strum1_debouncer.attach(strum1_pin);
	strum1_debouncer.interval(50);

#ifdef DEBUG
	Serial.begin(115200);
#endif

	setup_WebSocket();

}

void setup_WebSocket()
{
	Ethernet.begin(mac, ip);

#ifdef DEBUG
	Serial.println(Ethernet.localIP());
#endif

	client.stop();

	if (client.connect(WEB_SOCKET_SERVER, WEB_SOCKET_PORT))
	{
#ifdef DEBUG
		Serial.println("Connected to : ");
		Serial.println(Ethernet.localIP());
#endif
	}
	else
	{
#ifdef DEBUG
		Serial.println("Connection Failed !");
#endif

		hang();	// hang on failure
	}

	if (websocket::clientHandshake(client, WEB_SOCKET_SERVER, "/ws") == websocket::Success_Ok)
	{
#ifdef DEBUG
		Serial.println("Hanshake Successful !");
#endif
	}
	else
	{
#ifdef DEBUG
		Serial.println("Handshake Failed !");
#endif

		hang();	// hang on failure
	}
	client.flush();
}

void loop()
{
	if (client.connected())
	{
		webSocketClient.dispatchEvents();

		boolean strum0_stateChanged = strum0_debouncer.update();
		boolean strum1_stateChanged = strum1_debouncer.update();

		int strum0_state = strum0_debouncer.read();
		int strum1_state = strum1_debouncer.read();

		if (strum0_stateChanged || strum1_stateChanged)
		{
			if (strum0_state == LOW || strum1_state == LOW)
			{
				String data = String();

				data = String(ID_INSTRUMENT) + "|" + String(analogRead(touch0_pin) < THRESHOLD_INSTRUMENT) + "|" + String(analogRead(touch1_pin) < THRESHOLD_INSTRUMENT) + "|"
					+ String(analogRead(touch2_pin) < THRESHOLD_INSTRUMENT) + "|" + String(analogRead(touch3_pin) < THRESHOLD_INSTRUMENT);

				webSocketClient.sendData(data.c_str());

				client.flush();
			}
		}


	}
	else
	{
#ifdef DEBUG
		Serial.println("Connection Closed !");
#endif

		hang();
	}
}
