

void setup_WebSocket()
{
	Ethernet.begin(mac, ip);

#ifdef DEBUG
	Serial.println(Ethernet.localIP());
#endif

	client.stop();

	if (client.connect(WEB_SOCKET_SERVER, WEB_SOCKET_PORT))
	{
#ifdef DEBUG
		Serial.println("Connected to : ");
		Serial.println(Ethernet.localIP());
#endif
	}
	else
	{
#ifdef DEBUG
		Serial.println("Connection Failed !");
#endif

		hang();	// hang on failure
	}

	if (websocket::clientHandshake(client, WEB_SOCKET_SERVER, "/ws") == websocket::Success_Ok)
	{
#ifdef DEBUG
		Serial.println("Hanshake Successful !");
#endif
	}
	else
	{
#ifdef DEBUG
		Serial.println("Handshake Failed !");
#endif

		hang();	// hang on failure
	}
	client.flush();
}
static void hang()
{
	delay(1000);
	setup_WebSocket();
}
