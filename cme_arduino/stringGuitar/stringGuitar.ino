#include <Metro.h>
#define THRESHOLD 4
#define PICKUP_PIN 0
#define ALPHA 0.03
#define ALPHA2 0.05

float acumulator = 0;
float prevValue = 0;
Metro serialMetro = Metro(50);
Metro dataMetro = Metro(10);
int value;
unsigned long prevTime;

void setup()
{
	Serial.begin(115200);
	prevTime = millis();
}

signed int myAnalogRead()
	{

	  while(!(ADCSRA & 0x10)); // wait for adc to be ready
      ADCSRA = 0xf5; // restart adc
      byte m = ADCL; // fetch adc data
      byte j = ADCH;
      int k = (j << 8) | m; // form into an int
      k -= 0x0200; // form into a signed int
      k <<= 6; // form into a 16b signed int
      return k;
	}

void loop()
{
	if (dataMetro.check())
	{
		//value = analogRead(PICKUP_PIN);
		value = myAnalogRead();
		prevValue = prevValue * (1 - ALPHA) + value * (ALPHA);
		if (value > prevValue)
			acumulator = acumulator * (1 - ALPHA2) + value * (ALPHA2);
		else
			acumulator = acumulator* (1 - ALPHA) + value * (ALPHA);
		
		if ((acumulator - prevValue>1.3))
		{
			if (millis() - prevTime > 175)
			{
				Serial.print("acumulator : ");
				Serial.println(acumulator);
				prevTime = millis();
			}
			
			prevValue = acumulator;
			
		}
		

	}
		

	if (acumulator >= THRESHOLD )
	{
		
	}

	if (serialMetro.check())
	{
		
		

		prevValue = acumulator;
	}
	
	//delay(10);
}
