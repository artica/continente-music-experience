
//
// websockets server
//
//var midi = require('midi');
var ws = require('websocket.io');
var server = ws.listen(3001);


var global_counter = 0;
var active_conn = {};

server.on('connection', function (socket) {

	var id = global_counter++;
	active_conn[id] = [];
	active_conn[id]['socket'] = socket;
	active_conn[id]['callerid'] = null;
	active_conn[id]['dupe'] = false;
	
    socket.id = id; 

	socket.send('welcome');
	console.log("one more tune!");

	socket.on('message', function (data) {
	
		console.log('received this message: '+data);
		
		for (conn in active_conn) {
			active_conn[conn]['socket'].send( data );
		}
	
	});

	socket.on('close', function () {
	});

	socket.on('error', function (exc) {
		console.log("ignoring ws error exception: " + exc);
		if (active_conn[id]) {
			var thisid = active_conn[id]['socket'];
			if (active_conn[thisid]) {
				//expunge(active_conn[thisid]);
			}
		}
	});

	function expunge(conn) {
		active_conn[conn]['socket'].close();
		delete conn['socket'];
		active_conn[conn]['dupe'] = true;
		//delete active_conn[conn];
	}
	
});


/*
var input_mpc = new midi.input();
var hrtime_mpc1 = process.hrtime();
var hrtime_mpc2 = process.hrtime();
var hrtime_mpc3 = process.hrtime();
var hrtime_mpc4 = process.hrtime();
var mpc1 = 0;
var mpc2 = 0;
var mpc3 = 0;
var mpc4 = 0;
var mpc_appID = 2;

var mswait = 0.100;

function broadcast_mpc() {
	var data = mpc_appID + '|' + mpc1 + '|' + mpc2 + '|' + mpc3 + '|' + mpc4;
	 
	for (conn in active_conn) {
		active_conn[conn]['socket'].send( data );
	}
}

input_mpc.on('message', function(deltaTime, message) {
	//console.log('new message on input mpc');
	//console.log(message[0] + ' ' + message[1]);
  	
  	switch(message[0]) {
  		case 153: 
  			switch(message[1]) {
  				case 48:
				case 41:
				case 37:
				case 36: {
						var t = process.hrtime(hrtime_mpc1);
						var inseconds = (t[0]) + (t[1] /1e9);
	
						if (inseconds > mswait) {
							hrtime_mpc1 = process.hrtime();
							console.log('left hit');
							mpc1 = 1;
							broadcast_mpc();
						} else {
							//console.log('ignore other hits');
						}
					}
  				break;
  				
  				case 50:
  				case 42:
  				case 40:
  				case 38: {
						var t = process.hrtime(hrtime_mpc2);
						var inseconds = (t[0]) + (t[1] /1e9);
	
						if (inseconds > mswait) {
							hrtime_mpc2 = process.hrtime();
							console.log('mid left hit');
							mpc2 = 1;
							broadcast_mpc();
						} else {
							//console.log('ignore other hits');
						}
					}
  				break;
  				
  				case 52:
  				case 72:
  				case 39:
  				case 43: {
						var t = process.hrtime(hrtime_mpc3);
						var inseconds = (t[0]) + (t[1] /1e9);
	
						if (inseconds > mswait) {
							hrtime_mpc3 = process.hrtime();
							console.log('mid right hit');
							mpc3 = 1;
							broadcast_mpc();
						} else {
							//console.log('ignore other hits');
						}
					}
  				break;
  				
  				case 53:
  				case 76:
  				case 45:
  				case 44: {
						var t = process.hrtime(hrtime_mpc4);
						var inseconds = (t[0]) + (t[1] /1e9);
	
						if (inseconds > mswait) {
							hrtime_mpc4 = process.hrtime();
							console.log('right hit');
							mpc4 = 1;
							broadcast_mpc();
						} else {
							//console.log('ignore other hits');
						}
					}
  				break;
  				
  				default:
  					console.log('unassigned buttons?!');
  				break;
  			}
  		break;
  		case 137: // note off
  			switch(message[1]) {
  				case 48:
				case 41:
				case 37:
				case 36: {
						mpc1 = 0;
						broadcast_mpc();
					}
  				break;
  				
  				case 50:
  				case 42:
  				case 40:
  				case 38: {
						mpc2 = 0;
						broadcast_mpc();
					}
  				break;
  				
  				case 52:
  				case 72:
  				case 39:
  				case 43: {
						mpc3 = 0;
						broadcast_mpc();
					}
  				break;
  				
  				case 53:
  				case 76:
  				case 45:
  				case 44: {
						mpc4 = 0;
						broadcast_mpc();
					}
  				break;
  				
  				default:
  					console.log('unassigned buttons on mpc?!\ndisconnect and reconnect the device\nrelaunch the websocket midi server');
  				break;
  			}
  		break;
  	}
});



var input_keyb = new midi.input();
var hrtime_keyb1 = process.hrtime();
var hrtime_keyb2 = process.hrtime();
var hrtime_keyb3 = process.hrtime();
var hrtime_keyb4 = process.hrtime();
var keyb1 = 0;
var keyb2 = 0;
var keyb3 = 0;
var keyb4 = 0;
var keyb_appID = 3;
var leftkey = 35;
var calibrated = false;

function broadcast_keyb() {
	var data = keyb_appID + '|' + keyb1 + '|' + keyb2 + '|' + keyb3 + '|' + keyb4;
	 
	for (conn in active_conn) {
		active_conn[conn]['socket'].send( data );
	}
}

input_keyb.on('message', function(deltaTime, message) {
	//console.log('new message on input keyboard');
	//console.log(message);
  	//parser.parseByte(message);
  	//console.log(message[0] + ' ' + message[1]);
	
	if (!calibrated) {
		leftkey = message[1]-1;
		calibrated = true;
	}
  	
  	switch(message[0]) {
  		case 144: //note on
  			if ((message[1] > leftkey) && (message[1] < leftkey+6)) {
  				var t = process.hrtime(hrtime_keyb1);
				var inseconds = (t[0]) + (t[1] /1e9);
				if (inseconds > mswait) {
					hrtime_keyb1 = process.hrtime();
					console.log('first section hit');
					keyb1 = 1;
					broadcast_keyb();
				} else {
					//console.log('ignore other hits');
				}
  			} else if ((message[1] > leftkey+5) && (message[1] < leftkey+13)) {
  				var t = process.hrtime(hrtime_keyb2);
				var inseconds = (t[0]) + (t[1] /1e9);
				if (inseconds > mswait) {
					hrtime_keyb2 = process.hrtime();
					console.log('second section hit');
					keyb2 = 1;
					broadcast_keyb();
				} else {
					//console.log('ignore other hits');
				}
  			} else if ((message[1] > leftkey+12) && (message[1] < leftkey+20)) {
  				var t = process.hrtime(hrtime_keyb3);
				var inseconds = (t[0]) + (t[1] /1e9);
				if (inseconds > mswait) {
					hrtime_keyb3 = process.hrtime();
					console.log('third section hit');
					keyb3 = 1;
					broadcast_keyb();
				} else {
					//console.log('ignore other hits');
				}
  			} else if ((message[1] > leftkey+19) && (message[1] < leftkey+29)) {
  				var t = process.hrtime(hrtime_keyb4);
				var inseconds = (t[0]) + (t[1] /1e9);
				if (inseconds > mswait) {
					hrtime_keyb4 = process.hrtime();
					console.log('fourth section hit');
					keyb4 = 1;
					broadcast_keyb();
				} else {
					//console.log('ignore other hits');
				}
  			} else {
  				console.log('unassigned keys?!');
  			}
  		break;
  		case 128: // note off
  			if ((message[1] > leftkey) && (message[1] < leftkey+6)) {
  				keyb1 = 0;
				broadcast_keyb();
  			} else if ((message[1] > leftkey+5) && (message[1] < leftkey+13)) {
  				keyb2 = 0;
				broadcast_keyb();
  			} else if ((message[1] > leftkey+12) && (message[1] < leftkey+20)) {
  				keyb3 = 0;
				broadcast_keyb();
  			} else if ((message[1] > leftkey+19) && (message[1] < leftkey+29)) {
  				keyb4 = 0;
				broadcast_keyb();
  			} else {
  				console.log('unassigned keys?! did you screw up the calibration?');
  			}
  		break;
  	}
});

var portcount = input_mpc.getPortCount();
var mpcport = -1;
var keybport = -1;

for (var i=0; i < portcount; i++) {
	console.log( i + ': ' + input_mpc.getPortName(i));
	if (input_keyb.getPortName(i) == 'MIDISTART MUSIC 25') keybport = i;
	if ( (input_mpc.getPortName(i) == 'MIDIIN2 (padKONTROL)') || 
		 (input_mpc.getPortName(i) == 'padKONTROL PORT A')
		)  mpcport = i;
}

console.log(mpcport + ' ' + keybport);

if ((mpcport!=-1) && (keybport != -1)) {
	if (input_mpc.getPortName(mpcport)) {
		console.log('connecting mpc to port '+mpcport);
		input_mpc.openPort(mpcport);
	}
	if (input_keyb.getPortName(keybport)) {
		console.log('connecting keyb to port '+keybport);
		input_keyb.openPort(keybport);
	}
	console.log('\nyou need to calibrate the midi keyboard\npress the leftmost key first and thats it\n\n');
} else {
	console.log('did you forget to plug both midi devices?\nyou\ll have to restart the websocket midi server once they are plugged in');
}

*/
