

var ws = require('websocket.io');
var server = ws.listen(3001);


var global_counter = 0;
var active_conn = {};

server.on('connection', function (socket) {

	var id = global_counter++;
	active_conn[id] = [];
	active_conn[id]['socket'] = socket;
	active_conn[id]['callerid'] = null;
	active_conn[id]['dupe'] = false;
	
    socket.id = id; 

	socket.send('welcome');
	console.log("one more tune!");

	socket.on('message', function (data) {
	
		console.log('received this message: '+data);
		
		for (conn in active_conn) {
			active_conn[conn]['socket'].send( data );
		}
	
	});

	socket.on('close', function () {
	});

	socket.on('error', function (exc) {
		console.log("ignoring ws error exception: " + exc);
		if (active_conn[id]) {
			var thisid = active_conn[id]['socket'];
			if (active_conn[thisid]) {
				//expunge(active_conn[thisid]);
			}
		}
	});

	function expunge(conn) {
		active_conn[conn]['socket'].close();
		delete conn['socket'];
		active_conn[conn]['dupe'] = true;
		//delete active_conn[conn];
	}
	
	setInterval(
		function broadcast() {
			var data = Math.floor(Math.random()*4) + '|' + Math.floor(Math.random()*2) + '|' + Math.floor(Math.random()*2) + '|' + Math.floor(Math.random()*2) + '|' +Math.floor(Math.random()*2);
			console.log(data);
			for (conn in active_conn) {
				active_conn[conn]['socket'].send( data );
			}
		}
		, 10);
	
});

