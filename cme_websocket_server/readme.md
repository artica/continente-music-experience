
## Install

* install [node.js](http://nodejs.org/), make sure npm is in your path (should be automatically)
* launch command line and go to /cme_websocket_server/
* execute this line in your command line to install websocket.io dependency: `npm install websocket.io`
* execute this line in your command line to install midi dependency: `npm install midi`
* On Windows, midi has dependencies of python27 and Microsoft Visual Studio 2012 C++ Express. You might also need to execute `SET VisualStudioVersion=11.0` before `npm install midi`


## Run

* launch command line and go to /cme_websocket_server/
* start the node server: `node server.js`

