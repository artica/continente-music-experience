

Game.prototype.initGameScreen = function() {

	var my = this;
	
	this.score = 0;
	this.scorePercentage = 0;
	this.scoreMultiplier = 1;
	
	this.gameScreenBG();
	
	requestAnimFrame( animate );
	
	var stage = this.stage;
	var renderer = this.renderer;	

	var thisSong = this.songs[this.selectedSong];

	// add background
	var bga = this.images[14];
	var bgsa = new PIXI.Sprite(bga);
	bgsa.position.x = 0;
	bgsa.position.y = 0;
	stage.addChild(bgsa);
	
	// add background second layer (for fft effect)
	var bgb = this.images[15];
	var bgsb = new PIXI.Sprite(bgb);
	bgsb.position.x = 0;
	bgsb.position.y = 0;
	bgsb.alpha = 0.0;	
	stage.addChild(bgsb);
	
	var alphaDropper = 0.1;
	
	var d = new Date();
	var startTime = d.getTime();

	// add guiding lines and action box
	var linesT = this.images[17];
	var lines = new PIXI.Sprite(linesT);
	lines.position.x = 0;
	lines.position.y = 0;
	stage.addChild(lines);
	
	// add all graphic elements from the tracks and nodes of the song
	game.songs[game.selectedSong].initStage();

	// add logo
	var logoT = this.images[18];
	var logo = new PIXI.Sprite(logoT);
	logo.position.x = 0;
	logo.position.y = 0;
	stage.addChild(logo);
	
	function animate() {
		
		// cancel the animation
		if (my.state != my.gameScreen) {
			for (var i = stage.children.length - 1; i >= 0; i--) {
				stage.removeChild(stage.children[i]);
			};
			return;
		}

	    // loop the animation
	    requestAnimFrame( animate );

	    // flash spotlight layer with fft response
	    var d = new Date();
	    var n = d.getTime();
		var diffTime = n - startTime;
		startTime = n;
	    var data = thisSong.background.getFFT();
		if (data[thisSong.syncfreq] > 20) alphaDropper = alphaDropper * 2;
		if (alphaDropper > 1.0) alphaDropper = 1.0;
		alphaDropper = alphaDropper - (diffTime / 500);
		if (alphaDropper < 0.1) alphaDropper = 0.1;	
		bgsb.alpha = alphaDropper;

	    // animate the rest
	    thisSong.animateStage();

	    // render
	    renderer.render(stage);
	}
	
	// start playing
	thisSong.playpause();
	
	game.playSfx(5);
	
};

Game.prototype.gameScreenBG = function() {
	console.log('game screen bg');
	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs foreground";
		
		// score face
		var dom_scoreface = document.createElement("div");
		dom_scoreface.className = "scoreface_game";
		dom.appendChild(dom_scoreface);
		var dom_imageresultoverlay = document.createElement("div");
		dom_imageresultoverlay.setAttribute("id", "scoreface");
		dom_imageresultoverlay.className = "scoreface_overlay_game";
		dom.appendChild(dom_imageresultoverlay);
		
		// score value
		var dom_scorevalue = document.createElement("div");
		dom_scorevalue.setAttribute("id", "score");
		dom_scorevalue.className = "scorevalue_game";
		dom_scorevalue.innerHTML = '0';
		dom.appendChild(dom_scorevalue);
		
		// score multiplier
		var dom_scoremulvalue = document.createElement("div");
		dom_scoremulvalue.setAttribute("id", "scoremul");
		dom_scoremulvalue.className = "scoremulvalue_game";
		//dom_scoremulvalue.innerHTML = '';
		dom.appendChild(dom_scoremulvalue);
		
		// track name
		var dom_trackname = document.createElement("div");
		dom_trackname.className = "trackname_game";
		dom_trackname.innerHTML = game.songs[game.selectedSong].songname;
		dom.appendChild(dom_trackname);
		
		/* EDITOR
		var dom_tl = document.getElementById('triggerline');
		if (!dom_tl) this.createTriggerLine();
		*/
	}
};

/* EDITOR
Game.prototype.createTriggerLine = function() {

	var dom_tl = document.createElement("div");
	dom_tl.setAttribute("id", "triggerline");
	document.body.appendChild(dom_tl);
	
	var dom_time = document.createElement("div");
	dom_time.setAttribute("id", "time");
	dom_time.innerHTML = '0';
	dom_tl.appendChild(dom_time);
	
	var dom_im = document.createElement("div");
	dom_im.setAttribute("id", "importexport");
	dom_tl.appendChild(dom_im);

	var dom_bt = document.createElement("button");
	dom_bt.onclick = this.saveTextAsFile;
	dom_bt.innerHTML = 'Export';
	dom_im.appendChild(dom_bt);
	
	var dom_in = document.createElement("input");
	dom_in.setAttribute("id", "fileToLoad");
	dom_in.setAttribute("type", "file");
	dom_in.onchange = this.loadFileAsText;
	dom_in.setAttribute("value", "Import");
	dom_im.appendChild(dom_in);

};
*/

Game.prototype.updateScore = function(hit) {
	if (hit != -1) {
		var max = this.pickwindows.length;
		this.score += Math.ceil(max / (hit+1)) * this.pickmultiplier;
	} else {
		this.score -= 5;
		if (this.score < 0) this.score = 0;
	}
};

Game.prototype.interactWithGame = function(data) {
	if ((game.selectedSong != -1) && (game.selectedSong < game.songs.length)) 
	{
		var thisSong = game.songs[game.selectedSong];
		var msg = data.split('|');
		var sender = msg[0];
		if (msg.length == 5) {
			for (var i=1; i<5; i++) {
				if (msg[i] == '1') {
					var yref = thisSong.getLastPositionPixels();
					var hit = thisSong.tracks[i-1 + sender*4].testPick(yref, game.pickwindows);

					game.updateScore(hit);

					game.scorePercentage = 2.5 * game.score * game.scoreMultiplier / thisSong.maxscore;
					if (game.scorePercentage > 1) game.scorePercentage = 1.0;
										
					var dom_score = document.getElementById('score');
					if (dom_score) {
						if (hit == -1) dom_score.style.color = 'red';
							else dom_score.style.color = 'white';
						dom_score.innerHTML = game.score;
					}
				
					if (game.scoreMultiplier > 1)
					{
						var dom_scoremul = document.getElementById('scoremul');
						if (dom_scoremul) {
							dom_scoremul.className = "scoremulvalue_game x"+(game.scoreMultiplier);
							dom_scoremul.innerHTML = 'x'+game.scoreMultiplier;
						}
					}
									
					var dom_scoreface = document.getElementById('scoreface');
					if (dom_scoreface) {
						var maxHeight = 204; 
						var newHeight = parseInt(maxHeight * game.scorePercentage, 10);
						dom_scoreface.style.clip = 'rect(' + (maxHeight - newHeight) + 'px, 204px, 204px, 0px)';
					}
				}
			}
		}
	} else {
		console.log('invalid song id');
	}
};

Game.prototype.keybGame = function(e) {
	
	var thisSong = game.songs[game.selectedSong];
	if (thisSong && thisSong.tracks.length > 0) {
		switch (e.keyCode) {

			case 27: // escape
				if (thisSong.isPlaying) thisSong.playpause();
				game.loadState(game.trackSelectionScreen);
			break;
			
			case 49: { // 1
				var idx = 0;//game.instrument_visual_order[game.instrument_default_index[0]];
				var mute = thisSong.instruments[idx].toggleMute();
			}
			break;
			case 50: { // 2
				var idx = 1;//game.instrument_visual_order[game.instrument_default_index[1]];
				var mute = thisSong.instruments[idx].toggleMute();
			}
			break;
			case 51: { // 3
				var idx = 2;//game.instrument_visual_order[game.instrument_default_index[2]];
				var mute = thisSong.instruments[idx].toggleMute();
			}
			break;
			case 52: { // 4
				var idx = 3;//game.instrument_visual_order[game.instrument_default_index[3]];
				var mute = thisSong.instruments[idx].toggleMute();
			}
			break;

			/*	EDITOR
			case 32: { // space bar
				console.log('play pause?!');
				thisSong.playpause();
			}
			break;
				
			case 86: { // v
				this.interactWithGame('3|1|0|0|0');
			}
			break;
			case 66: { // b
				this.interactWithGame('3|0|1|0|0');
			}
			break;
			case 78: { // n
				this.interactWithGame('3|0|0|1|0');
			}
			break;
			case 77: { // m
				this.interactWithGame('3|0|0|0|1');
			}
			break;
			*/
		}
	}
	
};
