
function Track(xPos, imageID, parent, instrumentRef) {

	this.xPosPercentage = xPos;
	this.width = 75;
	this.imageID = imageID;
	this.images = parent.images;
	this.parent = parent;
	this.instrument = instrumentRef;
	this.peaks = this.parent.peaks;
	this.length = this.parent.getDurationPixels();

	this.nodes = [];
	this.selectedNode = -1;
	this.bottomNode = -1;
	this.topNode = -1;

	this.bottomPad = 190; // of sound
	this.trackID = uid.get_uid();

	this.stage = game.stage;
	this.activationImageRef = null;
	this.activationTime = null;
	this.ellapsedTime = 100; //ms

	this.ch = game.ch;
	this.cw = game.cw;
	
	this.calcIndexes();

}

Track.prototype = {
	
	initStage: function(imageID) {
		for (var i=0; i<this.nodes.length; i++) {
			this.nodes[i].initStage();
		}
		
		// add images of noteon / noteoff
		var thisx = Math.ceil(this.xPosPercentage*this.cw);
		this.activationImageRef = new PIXI.Sprite(this.images[imageID]);
		//console.log(imageID + ' ' + thisx);
		this.activationImageRef.position.x = thisx-20;
		this.activationImageRef.position.y = 910;
		this.activationImageRef.alpha = 0.35;

		this.stage.addChild(this.activationImageRef);
	},
	
	animateStage: function(yRef, time) {
	
		this.calcBottomNode( yRef );
		this.calcTopNode( yRef + this.ch );
		
		var nodesyref = yRef + this.ch - this.bottomPad;
		for (var i=this.bottomNode; i<this.topNode; i++) {
			this.nodes[i].animateStage(Math.ceil(this.xPosPercentage*this.cw), nodesyref);
		}
		
		if (time > this.activationTime + this.ellapsedTime) {
			this.activationImageRef.alpha = 0.35;
		} else {
			this.activationImageRef.alpha = 1.0;
		}
		
	},
	
	addNode: function(y) {
		var newNode = new TrackNode(y, this);

		// go through nodes to find position where to slot this node
		// array must always be y-sorted (position 0 has lowest y)	
		var insertPlace = 0;
		for (var i = 0; i < this.nodes.length; i++) {
			if (y > this.nodes[i].yPos) {
				insertPlace = i+1;
			}
		}
		this.nodes.splice(insertPlace, 0, newNode);
		
		return newNode;
	},
	
	calcIndexes: function() {
		this.bottomNode = 0;
		var yRef = this.parent.getLastPositionPixels(); // retrieved yRef is on bottom of the screen
		this.calcBottomNode(yRef);
		this.topNode = this.bottomNode;
		this.calcTopNode(yRef + this.ch);
		//console.log('node limits: ' + this.bottomNode + ' ' + this.topNode);
	},
	
	calcBottomNode: function(yRef) {
	
		if (this.nodes.length == 0) return;
		if (!this.nodes[this.bottomNode]) return;
	
		// check if bottomnode's position is under the bottom screen bounds (negative y axis)	
		if (this.nodes[this.bottomNode].yPos > yRef - this.bottomPad*2) {
			// everything is fine
			return;
		} else {
			// it's already down under, let's see if we can find a new bottomnode
			// go recurringly up until its within bounds or we're out of nodes
			for (var i=this.bottomNode; i < this.nodes.length; i++) {
				this.nodes[i].hideSprite();
				if (this.nodes[i].yPos > yRef) {
					this.bottomNode = i;
					//console.log(i + ' should be better bottomnode: ' + this.nodes[i].yPos + ' for ' + yRef);
					return;
				}
			}
		}
	},
	
	calcTopNode: function(yRef) {
	
		// check if there is a next topnode within range to consider
		for (var i=this.topNode; i<this.nodes.length; i++) {
			//console.log(i + ' ' + this.nodes[i].yPos + ' ' + yRef);

			if (this.nodes[i].yPos < yRef) {
				// if there is a better top node keep going
			} else {
				this.topNode = i;
				// else it means we already have found the best one
				return;
			}
		}
		
		// worst case scenario, if we didnt get any hits
		this.topNode = this.nodes.length;
	},
	
	sortNodes: function() {
		this.nodes.sort(function(a,b){return a.yPos - b.yPos});
	},
	
	deleteSelectedNode: function() {
		if (this.selectedNode != -1) {
			for (var i = this.bottomNode; i < this.topNode; i++) {
				if (this.nodes[i].nodeID == this.selectedNode) {
					this.nodes.splice(i, 1);
					this.topNode--;
					return;
				}
			}
		}
	},

	testPick: function(wpos, pwindows) {
	
		console.log('testing ' + wpos + ' ' + this.nodes[this.bottomNode].yPos);
		
		// get activation time
		var d = new Date();
		this.activationTime = d.getTime();
		
		// get position reference for test pick
		var refpos = wpos + this.bottomPad - 160;

		for (var i = this.bottomNode; i < this.topNode; i++) {
			if (!this.nodes[i].isLit) {
				var ypos = this.nodes[i].yPos;
				for (var j=0; j < pwindows.length; j++) {
					var ptop = refpos - pwindows[j];
					var pbot = refpos + pwindows[j];
					if ((ypos > ptop) && (ypos < pbot)) {
						//console.log('hit ' + j);
						this.nodes[i].isLit = true;
						
						if (this.nodes[i].isSpecial) {
							game.scoreMultiplier++;
						}

						// improve earbility						
						this.instrument.increaseGain(0.05, this.activationTime);
						
						return j;
					}
				}
			}
		}
		
		// missed hit
		// reduce earbility						
		this.instrument.decreaseGain(0.5);
		
		return -1;
	},


/* EDITOR
	//
	// mouse interaction
	//
	
	checkBounds: function(eX) {
		return (
			(eX > this.xPosPercentage * this.cw - this.width * 0.5) &&
		    (eX < this.xPosPercentage * this.cw + this.width * 0.5)
		);
	},

	onmousedown: function(e) {
		if (this.checkBounds(e.pageX))
		{	
			//console.log('mouse down on track ' + this.trackID);
			for (var i=this.bottomNode; i<this.topNode; i++) {
				var hit = this.nodes[i].onmousedown(e);
				// if there is a node being hit it will update it's parent.selectedNode 
			}
			
			if (this.selectedNode == -1) {
				var node = this.addNode(this.ch - e.pageY + e.ref - this.bottomPad);
				this.selectedNode = node.nodeID;
				this.calcIndexes();
			}
		}
		return false;
	},
	
	onmousemove: function(e) {
		if (this.checkBounds(e.pageX))
		{
			if(!e.altKey) {
				//console.log('mouse move on track ' + this.trackID + ' with refY ' + e.ref);
				for (var i = this.bottomNode; i < this.topNode; i++) {
					if (this.nodes[i].nodeID == this.selectedNode) {
						this.nodes[i].onmousemove(e);
						this.sortNodes();
					}
				}
			}
		} else {
		
			// dragging a selected node out of bounds
			if (this.selectedNode != -1) {
				//console.log('selected node ' + this.selectedNode + ' is out of bounds ' + e.ref + ' ' + this.ch);

				// if new posX is over another lane 
				for (var i=0; i<this.parent.tracks.length; i++) {
					if (this.parent.tracks[i].checkBounds(e.pageX)) {
					
						//console.log('we could add it to ' + this.parent.tracks[i]);
						
						// add it there
						var myY = this.ch - e.pageY + e.ref - this.bottomPad;
						if(e.altKey) {
							// snap to same position on new lane
							for (var j = this.bottomNode; j < this.topNode; j++) {
								if (this.nodes[j].nodeID == this.selectedNode) {
									myY = this.nodes[j].yPos;
								}
							}
							
						}
						//console.log('adding node to ' + myY); //-74 instead of 969
						
						// move with mouse
						var node = this.parent.tracks[i].addNode(myY);
						this.parent.tracks[i].selectedNode = node.nodeID;
						this.parent.tracks[i].calcIndexes();
						
						// remove it from this track
						this.deleteSelectedNode();
						this.selectedNode = -1;
					}
				}

			}
			
		}
		return false;
	},
	
	
	
	//
	// export JSON
	//
	
	exportJSON: function() {
		var output = [];
		for (var i=0; i<this.nodes.length; i++) {
			output.push( this.nodes[i].yPos );
		}
		return output;
	}
*/
}
