Game.prototype.rankingScreenBG = function() {

	var newScore = [];
	newScore[0] = '';
	newScore[1] = game.scorePercentage;
	newScore[2] = game.songs[game.selectedSong].songname;
	var name = false;

	var inputbox = document.getElementById('inputbox');
	if (inputbox) {
		newScore[0] = inputbox.value;
		name = true;
	} else {
		// coming from splash screen
		//alert('no team name?!');
	}

	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs rankingScreen";

		/*var dom_logo = document.createElement("div");
		dom_logo.className = "fs logo_result";
		dom.appendChild(dom_logo);
		*/
		var dom_title = document.createElement("div");
		dom_title.className = "title_ranking";
		dom_title.innerHTML = 'Classificações';
		dom.appendChild(dom_title);
		
		var pos = -1;
		
		if (name) {
			// insert newScore by newTeam into hiscores
			for (pos=0; pos<this.hiscores.length; ) {
				if (newScore[1] > this.hiscores[pos][1]) {
					break;
				} else {
					pos++;
				}
			}
			console.log('inserting ' + newScore + ' in ' + pos + ' of ' + this.hiscores.length);
			this.hiscores.splice( pos, 0, newScore );
		
			// make sure it's stored on localstorage
			window.localStorage.setItem('hiscores', JSON.stringify(this.hiscores));
		}
				
		// display rankings
		var max = 6;
		if (this.hiscores.length > 6) max = 6;
			else max = this.hiscores.length;
			
		for (var i=0; i < max; i++) {
		
			var highlight = '';
			if ((name) && (pos == i)) highlight = "highlight_";
			
			var name = document.createElement("div");
			name.className = highlight + "names_ranking";
			name.style.top = (26 + i*6.5) + '%'; 
			name.innerHTML = this.hiscores[i][0];
			dom.appendChild(name);
			
			var songname = document.createElement("div");
			songname.className = highlight + "songname_ranking";
			songname.style.top = (26 + i*6.5) + '%'; 
			songname.innerHTML = this.hiscores[i][2];
			dom.appendChild(songname);
			
			var result = document.createElement("div");
			result.className = highlight + "results_ranking";
			result.style.top = (26 + i*6.5) + '%'; 
			result.innerHTML = parseInt(this.hiscores[i][1] * 100, 10) + '%';
			dom.appendChild(result);
	
		}
		
		// if result was not listed, append it last		
		if ((name) && (pos >= max)) {
			var highlight = "highlight_";
			
			var name = document.createElement("div");
			name.className = highlight + "names_ranking";
			name.style.top = (26 + max*6.5) + '%'; 
			name.innerHTML = newScore[0];
			dom.appendChild(name);
			
			var songname = document.createElement("div");
			songname.className = highlight + "songname_ranking";
			songname.style.top = (26 + max*6.5) + '%'; 
			songname.innerHTML = newScore[2];
			dom.appendChild(songname);
			
			var result = document.createElement("div");
			result.className = highlight + "results_ranking";
			result.style.top = (26 + max*6.5) + '%'; 
			result.innerHTML = parseInt(newScore[1] * 100, 10) + '%';
			dom.appendChild(result);
		}
/*
		var dom_footnote = document.createElement("div");
		dom_footnote.className = "footnote_ranking";
		dom_footnote.innerHTML = 'Os teus pontos são decisivos para ajudar a Missão Sorriso';
		dom.appendChild(dom_footnote);
	*/	
	}
	
	game.playSfx(7);
	game.testBackgroundMusic();
};

Game.prototype.saveScores = function() {
	var textToWrite = JSON.stringify(game.hiscores);
	var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
	var fileNameToSaveAs = 'scores.txt';

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	if (window.webkitURL != null)
	{
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
	}
	else
	{
		// Firefox requires the link to be added to the DOM
		// before it can be clicked.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = game.destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}

	downloadLink.click();
};
	
Game.prototype.keybRanking = function(e) {
	console.log(e.keyCode);
	switch (e.keyCode) {
		case 13: // return
			game.loadState(game.splashScreen);
		break;
		case 83: // s
			console.log('saving scores?...');
			game.saveScores();
		break;
		case 76: //l
			console.log('clearing hiscore table');
			game.hiscores = [];
			window.localStorage.setItem('hiscores', JSON.stringify(game.hiscores));
			game.loadState(game.rankingScreen);
		break;
	}
};
