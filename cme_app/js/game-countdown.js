Game.prototype.initCountdownScreen = function() {
	
	var my = this;
	this.countdown3ScreenBG();
/*
	var stage = new PIXI.Stage();
	var renderer = new PIXI.WebGLRenderer(1920, 1080, null, true);//autoDetectRenderer(400, 300);
	document.body.appendChild(renderer.view);

	requestAnimFrame( animate );
	
	// bg
	var bgTexture = PIXI.Texture.fromImage("gfx/01_MSMX_splash/01_MSMX_splash_bg.png");
	var bgSprite = new PIXI.Sprite(bgTexture);
	stage.addChild(bgSprite);

	var texture = PIXI.Texture.fromImage("gfx/01_MSMX_splash/01_MSMX_splash_fx1.png");

	var d = new Date();
	var startTime = d.getTime();
		
	var spotlights = [];
	var nspotlights = 30;
	
	for (var i=0; i<nspotlights; i++) {

		spotlights[i] = new PIXI.Sprite(texture);
		spotlights[i].blendMode = PIXI.blendModes.SCREEN;

		spotlights[i].anchor.x = 0.5;
		spotlights[i].anchor.y = 0.0;

		spotlights[i].position.x = Math.random()*this.cw;
		spotlights[i].position.y = Math.random()*this.ch*0.1 - 200;
	
		spotlights[i].scale.x = 1.0 + Math.random()*1.5;
		spotlights[i].scale.y = 1.5 + Math.random()*2.5;
	
		spotlights[i].rotation = Math.PI*0.05;
		spotlights[i].thisOpening = 0.001 + Math.random()*0.002;
		spotlights[i].thisSpeed = 0.005 + Math.random()*0.005;
		spotlights[i].alpha = 0.25 + Math.random()*0.25;

		stage.addChild(spotlights[i]);
	}

	function animate() {
		
		// cancel the animation
		if (my.state != my.countdownScreen) {
			document.body.removeChild(renderer.view);
			stage = null;
			return;
		}
	    
	    // loop the animation
	    requestAnimFrame( animate );

		// apply changes to frame
		var d = new Date();
		var n = startTime - d.getTime();
		for (var i=0; i<spotlights.length; i++) {
	    	spotlights[i].rotation += Math.sin( n * spotlights[i].thisOpening ) * spotlights[i].thisSpeed;
	    }
	    // render
	    renderer.render(stage);
	}*/
}

Game.prototype.countdown3ScreenBG = function() {
	console.log('counting 3');
	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs splashScreenBG";
		
		var dom_count = document.createElement("div");
		dom_count.setAttribute("id","count");
		dom_count.className = "count_countdown";
		dom_count.innerHTML = '3';
		dom.appendChild(dom_count);
		
		if (this.countdownAnim) clearTimeout(this.countdownAnim);
		this.countdownAnim = setTimeout( function(){ game.countdown2ScreenBG(); }, 1000);
	}
	
	game.playSfx(4);
};
	
Game.prototype.countdown2ScreenBG = function() {
	var dom = document.getElementById("bgcontainer")
	if (dom) {

		var dom_count = document.getElementById('count');
		if (dom_count) {
			dom_count.innerHTML = '2';
		}
		
		if (this.countdownAnim) clearTimeout(this.countdownAnim);
		this.countdownAnim = setTimeout( function(){ game.countdown1ScreenBG(); }, 1000);
	}
	
	game.playSfx(4);
};
	
Game.prototype.countdown1ScreenBG = function() {
	var dom = document.getElementById("bgcontainer")
	if (dom) {

		var dom_count = document.getElementById('count');
		if (dom_count) {
			dom_count.innerHTML = '1';
		}
		
		if (this.countdownAnim) clearTimeout(this.countdownAnim);
		this.countdownAnim = setTimeout( function(){ game.loadState(game.gameScreen); }, 1000);
	}
	
	game.playSfx(4);
};

Game.prototype.keybCountdown = function(e) {
	switch (e.keyCode) {
		case 27: // escape
			   game.loadState(game.instructionsScreen);
		break;
	}
};
