
function Background(audioContext) {
	this.bufferSource = null;
	this.gainNode = null;
	this.ac = audioContext;	
	this.fft = null;
	this.fftsamples = 128;
}

Background.prototype = {
	create: function(buffer, volume) {
		this.fft = this.ac.createAnalyser();
		this.fft.fftSize = this.fftsamples;
		this.fft.connect(this.ac.destination);
	
		this.gainNode = this.ac.createGain();
		this.gainNode.gain.value = volume;
		this.gainNode.connect(this.fft);
					
		this.bufferSource = this.ac.createBufferSource();
		this.bufferSource.buffer = buffer;
		this.bufferSource.connect(this.gainNode);
		//console.log('creating instrument');
		
	},
	play: function(position) {
		//console.log('playing....');
		this.bufferSource.start( 0, position );	
	},
	stop: function() {
		this.bufferSource.stop(0);
	},
	getFFT: function() {
		var data = new Uint8Array(this.fftsamples);
		this.fft.getByteFrequencyData(data);
		return data;
	}
	
}
