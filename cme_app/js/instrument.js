
function Instrument(audioContext, xPosPercentage, imageID) {
	this.bufferSource = null;
	this.gainNode = null;
	this.ac = audioContext;
	
	this.stage = game.stage;
	this.xPosPercentage = xPosPercentage;
	this.cw = game.cw;
	this.images = game.images;
	this.imageID = imageID;
	this.sprite = null;
	this.spriteGlow = null;
	this.activationTime = null;
	this.ellapsedTime = 3000; //ms
	
	this.fft = null;
	this.fftsamples = 128;
	
	this.isMute = false;
}

Instrument.prototype = {
	create: function(buffer) {
	
		this.gainNode = this.ac.createGain();
		this.gainNode.gain.value = 1.0;
		this.gainNode.connect(this.ac.destination);
		//this.gainNode.connect(this.fft);
					
		this.bufferSource = this.ac.createBufferSource();
		this.bufferSource.buffer = buffer;
		this.bufferSource.connect(this.gainNode);
		//console.log('creating instrument');
		
	},
	play: function(position) {
		//console.log('playing....');
		this.bufferSource.start( 0, position );	
	},
	stop: function() {
		this.bufferSource.stop(0);
	},
	increaseGain: function(ammount, time) {
		if (this.isMute) return;
		
		if (time != null) {
			this.activationTime = time;
		}
		
		//console.log('increasing gain');
		if (this.gainNode) {
			this.gainNode.gain.value += ammount;
			if (this.gainNode.gain.value > 1) this.gainNode.gain.value = 1;
		}
	},
	decreaseGain: function(ammount) {
		if (this.isMute) return;
		
		this.activationTime = null;
		this.spriteGlow.alpha = 0.0;
		
		//console.log('lowering gain');
		if (this.gainNode) {
			this.gainNode.gain.value -= ammount;
			if (this.gainNode.gain.value < 0.1) this.gainNode.gain.value = 0.1;
		}
	},
	toggleMute: function() {
		if (this.isMute) {
			this.isMute = false;
			this.gainNode.gain.value = 1.0;
		} else {
			this.isMute = true;
			this.gainNode.gain.value = 0.0;
		}
		return this.isMute;
	},
	initStage: function() {
		console.log('adding instrument with ' + this.imageID);
		var thisx = Math.ceil(this.xPosPercentage*this.cw);
		this.sprite = new PIXI.Sprite(this.images[this.imageID]);
		this.sprite.position.x = thisx-12;
		this.sprite.position.y = 910;
		this.sprite.alpha = 1.0;
		this.stage.addChild(this.sprite);
		this.spriteGlow = new PIXI.Sprite(this.images[this.imageID+1]);
		this.spriteGlow.position.x = thisx-12;
		this.spriteGlow.position.y = 910;
		this.spriteGlow.alpha = 0.0;
		this.stage.addChild(this.spriteGlow);
	},
	animateStage: function(time) {
		//if (this.gainNode) {
					
			if (this.activationTime) {
				// might have glow
				if (time > this.activationTime + this.ellapsedTime) {
					// no glow
					this.spriteGlow.alpha = 0;
					//console.log('no more glow');
				} else {
					// glow
					var temp = 1.0 - ((time - this.activationTime) / this.ellapsedTime);
					this.spriteGlow.alpha = temp;
				}
				
			}
			
			this.sprite.alpha = this.gainNode.gain.value*2.0;
			
		//}
	},
	
}
