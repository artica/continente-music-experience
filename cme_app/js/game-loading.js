
Game.prototype.initLoadingScreen = function() {

	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs splashScreenBG";
		
		var dom_logo = document.createElement("div");
		dom_logo.className = "fs logo_splash";
		dom.appendChild(dom_logo);
		
		var dom_loading = document.createElement("div");
		dom_loading.setAttribute("id","loading");
		dom_loading.className = "loading";
		dom_loading.innerHTML = "Carregando jogo";
		dom.appendChild(dom_loading);
		
	}
	
	// give 2 seconds for dom to update before starting to load
	setTimeout( function(){ game.loadSong0(); }, 2000);

};

Game.prototype.loadSong0 = function() {
	var dom_loading = document.getElementById('loading');
	if (dom_loading) {
		dom_loading.innerHTML = "Carregando tema 1 de 3";
	}
	//console.log('loading song 0');
	game.loadSong( 0, function(){ game.loadSong1(); } );
};

Game.prototype.loadSong1 = function() {
	var dom_loading = document.getElementById('loading');
	if (dom_loading) {
		dom_loading.innerHTML = "Carregando tema 2 de 3";
	}
	game.loadSong( 1, function(){ game.loadSong2(); } );
};

Game.prototype.loadSong2 = function() {
	var dom_loading = document.getElementById('loading');
	if (dom_loading) {
		dom_loading.innerHTML = "Carregando tema 3 de 3";
	}
	game.loadSong( 2, 
		function(){ 
			game.connectWebSockets();
			game.loadState(game.splashScreen);
		} );
};

Game.prototype.loadSong = function(songID, cb) {

	if ( (songID > this.songs.length) || (songID == -1) ) {
		alert('trying to load unlisted song');
		return;
	}
		
	this.songs[songID].load( cb );
	
};
