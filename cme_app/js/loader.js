var Loader = function(waitfor, launch) {
	this.max = waitfor;
	this.count = 0;
	this.launch = launch;
}

Loader.prototype.increaseCounter = function() {
	this.count++;
	console.log('up to ' + this.count + ' of ' + this.max);
	if (this.count == this.max) {
		console.log('touch down');
		this.launch();
	}
}
