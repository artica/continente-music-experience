
var Game = function() {


	//
    // init websockets
    //

    this.websockets = 'ws://localhost:3001';
	this.ws = null;
    this.timeout = false;
    
    this.triggers = [];
    for (i = 0; i < 4*4; i++) {
    	this.triggers[i] = 0;
    }



	//
	// pixi.js stage
	//
	
	this.stage = new PIXI.Stage();
	this.renderer = new PIXI.autoDetectRenderer(1920, 1080, null, true);
	//this.renderer = new PIXI.WebGLRenderer(1920, 1080, null, true);
	document.body.appendChild(this.renderer.view);
	
	this.cw = 1920;
	this.ch = 1080;
	

	//
	// instrument order
	//
	
	this.instrument_visual_order = {
					drums:0,
					bass: 1,
					mpc:  2,
					keys: 3
				};
	console.log(this.instrument_visual_order);

	// DONT TOUCH THE ONE BELOW, ALTER THE ONE ABOVE
	this.instrument_default_index = ['drums','bass','mpc','keys'];
	this.instrument_default = {'drums':0, 'bass':1, 'mpc':2, 'keys':3};



	//
	// init background container
	//
	
	var dom_bg = document.createElement("div");
	dom_bg.setAttribute("id", "bgcontainer");
	document.body.appendChild(dom_bg);



	//
	// load images
	//
	
	this.images = [];
    this.images[0] = PIXI.Texture.fromImage("gfx/bolas/symbol.png");
	this.images[1] = PIXI.Texture.fromImage("gfx/bolas/symbol_hit.png");
	this.images[2] = PIXI.Texture.fromImage("gfx/bolas/pink.png");
	this.images[3] = PIXI.Texture.fromImage("gfx/bolas/pink_hit.png");
	this.images[4] = PIXI.Texture.fromImage("gfx/bolas/orange.png");
	this.images[5] = PIXI.Texture.fromImage("gfx/bolas/orange_hit.png");
	this.images[6] = PIXI.Texture.fromImage("gfx/bolas/green.png");
	this.images[7] = PIXI.Texture.fromImage("gfx/bolas/green_hit.png");
	this.images[8] = PIXI.Texture.fromImage("gfx/bolas/blue.png");
	this.images[9] = PIXI.Texture.fromImage("gfx/bolas/blue_hit.png");	
	
	this.images[10] = PIXI.Texture.fromImage("gfx/01_MSMX_splash/01_MSMX_splash_bg.jpg");
	this.images[11] = PIXI.Texture.fromImage("gfx/01_MSMX_splash/01_MSMX_splash_fx1.png");
	this.images[12] = PIXI.Texture.fromImage("gfx/03_MSMX_instrucoes/03_MSMX_instrucoes_BGc.png");
	this.images[13] = PIXI.Texture.fromImage("gfx/03_MSMX_instrucoes/03_MSMX_instrucoes_fx1.png");
	this.images[14] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/05_MSMX_jogo_3D_BGa.jpg");
	this.images[15] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/05_MSMX_jogo_3D_BGb.jpg");
	this.images[16] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/led_matrix.png");
	this.images[17] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/05_MSMX_jogo_3D_linhas.png");
	this.images[18] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/05_MSMX_jogo_3D_LOGO.png");
	
	this.images[19] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/pink.jpg");
	this.images[20] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/orange.jpg");
	this.images[21] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/green.jpg");
	this.images[22] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/blue.jpg");
	
	this.images[23] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_drums_feedbackOFF.png");
	this.images[24] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_drums_feedbackON.png");
	this.images[25] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_bass_feedbackOFF.png");
	this.images[26] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_bass_feedbackON.png");
	this.images[27] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_mpc_feedbackOFF.png");
	this.images[28] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_mpc_feedbackON.png");
	this.images[29] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_keys_feedbackOFF.png");
	this.images[30] = PIXI.Texture.fromImage("gfx/05_MSMX_jogo/instrumentos/05_MSMX_jogo_3D_instrumentos_icons_keys_feedbackON.png");
	


	//
	// init triggerline & countdown
	//
	
	this.countdownAnim = null;


	
	//
	// init score
	//
	
	this.score = 0;
	this.pickwindows = []; 	  // stores values in pixel range
	this.pickwindows[0] = 20; // perfect
	this.pickwindows[1] = 40; // good
	this.pickwindows[2] = 80; // medium
	this.pickmultiplier = 10; // score multiplier that applies to each window range
	this.numspecials = 16;
	this.scoreMultiplier = 1;
	
	
	
	//
	// game state
	//
		
	this.loadingScreen = 0;
	this.splashScreen = 1;
	this.trackSelectionScreen = 2;
	this.instructionsScreen = 3;
	this.countdownScreen = 4;
	//this.countdown2Screen = 5;
	//this.countdown1Screen = 6;
	this.gameScreen = 7;
	this.resultScreen = 8;
	this.rankingScreen = 9;

	this.state = 0;



	//
	// init hiscores
	//
	
	this.hiscores = JSON.parse(window.localStorage.getItem('hiscores'));
	
	if ((!this.hiscores) || (this.hiscores === '')) {
		this.hiscores = [];
		window.localStorage.setItem('hiscores', JSON.stringify(this.hiscores));
	}
	
	
	
	//
	// init songs
	//
	
    this.songs = [];
    this.songs[0] = new Song('24_7', 		'24/7', 	18, this);
    this.songs[1] = new Song('blue_tu', 	'Blue Tu', 	 8, this);
    this.songs[2] = new Song('throwback', 	'Throwback',18, this);
    
    this.selectedSong = 0;
    
    
    
    //
    // init sfx
    //
    
    this.sfxAc = new ( window.AudioContext || window.webkitAudioContext );
    this.sfxBufferList = null;
    var my = this;
    this.bufferLoader = new BufferLoader(
		this.sfxAc,
		[
		  'sfx/snip_24_7.wav', 		// needs to be snip of song 0
		  'sfx/snip_blue_tu.wav',	// needs to be snip of song 1
		  'sfx/snip_throwback.wav', // needs to be snip of song 2
		  // everything else can be whatever order you want
		  'sfx/applause.wav',		// result
		  'sfx/contagem.wav',		// 3, 2, 1
		  'sfx/contagem_3.wav',		// gamescreen start
		  'sfx/ecran.wav', 			// press splash screen arrow
		  'sfx/ecran_2.wav'			// all other transition screens
		],
		function finishedLoading(bufferList) {
			my.sfxBufferList = bufferList;
			console.log('sfx loaded');
		}
	);
	this.bufferLoader.load();
	this.selectedSnip = null;
	
	
	
	//
	// init background music
	//
	
	//this.backgroundMusicAC = new ( window.AudioContext || window.webkitAudioContext );
	this.bgSong = -1;
	this.bgLoop = null;
	


/* EDITOR
    //
    // add global listeners to scroll track position in gameScreen
    //
    
	window.document.addEventListener("mousewheel", grabScroll, false);
	window.document.addEventListener("DOMMouseScroll", grabScroll, false);
*/	
	
	
	//
	// init the app
	//
	//this.scorePercentage = Math.random();
	//setTimeout( function() { 
	//				game.connectWebSockets();
	//				game.loadSong( 0, function(){ game.loadState(game.resultScreen); } );
	//				game.loadState(game.splashScreen);
	//				game.loadState(game.rankingScreen);
	//			}, 1500);
	this.initLoadingScreen();
}



Game.prototype = {

	//
	// websockets stuff
	//

	connectWebSockets: function() {

		var obj = this;

		console.log("attempt to connect");
		this.timeout = false;
	
		obj.ws = new WebSocket(obj.websockets);        

		obj.ws.onopen = function() {
			console.log("opened socket");
			obj.ws.send("game|readyforduty");
		};

		obj.ws.onmessage = function(evt) {
			
			//console.log(evt.data);
			
			if (game) {
			
				switch(game.state) {
				
					case game.instructionsScreen: {
							game.interactWithInstructions(evt.data);
						}
						break;
					case game.gameScreen: {
							game.interactWithGame(evt.data);
						}
						break;
				}
			} else {
				console.log('no game, no message');
			}
		
		};

		obj.ws.onclose = function() {
			console.log("closed socket");
			if (!obj.timeout) obj.timeout = setTimeout(function(){obj.connectWebSockets()},5000);
		};
	
		obj.ws.onerror = function() {
			console.log("error on socket");
			if (!obj.timeout) obj.timeout = setTimeout(function(){obj.connectWebSockets()},5000);
		};
	},



	//
	// sfx
	//

	playSfx: function(id) {
		var src = this.sfxAc.createBufferSource();
		src.buffer = this.sfxBufferList[id];
		src.connect(this.sfxAc.destination);
		src.start( 0, 0 );
		return src;
	},
	
	
	
	//
	// background music
	//
	
	playNewBackgroundMusic: function() {
	
		console.log('playing new music');
		game.bgSong = Math.floor(Math.random()*game.songs.length);
		game.songs[game.bgSong].background.create(game.songs[game.bgSong].bufferList[0], 0.5);
		game.songs[game.bgSong].background.play(0);
		
		var songDuration = Math.floor( game.songs[game.bgSong].getDuration() * 1000);
		
		game.bgLoop = setTimeout( 
							game.playNewBackgroundMusic
							,songDuration	
						);
		
	},
	
	testBackgroundMusic: function() {
		console.log('testing bg music');
		if ((this.bgLoop === null) && (this.bgSong === -1)) {
			this.playNewBackgroundMusic();
		}
	},

	stopBackgroundMusic: function() {
		console.log('quiet please');
		if ((this.bgLoop !== null) && (this.bgSong !== -1)) {
			this.songs[this.bgSong].background.stop();
			clearTimeout(this.bgLoop);
			this.bgLoop = null;
			this.bgSong = -1;
		}
	},



	//
	// load state
	//

	loadState: function(index) {
		switch(index) {
			case this.loadingScreen:
				this.initLoadingScreen();
			break;
			case this.splashScreen:
				this.initSplashScreen();
			break;
			case this.trackSelectionScreen:
				this.trackSelectionScreenBG();
			break;
			case this.instructionsScreen: 
				this.initInstructionsScreen();
			break;
			case this.countdownScreen:
				this.initCountdownScreen();
			break;
			case this.gameScreen:
				this.initGameScreen();
			break;
			case this.resultScreen:
				this.initResultScreen();
			break;
			case this.rankingScreen:
				this.rankingScreenBG();
			break;
		}
		this.state = index;
	},
	
	
	
	getKeyByValue: function(value) {
		var thisvalue = 0;
		for( var prop in this.instrument_visual_order ) {
			if( this.instrument_visual_order[ prop ] === value ) {
				 thisvalue = prop;
			}
		}
		return game.instrument_default[thisvalue];
	},
	
/*	EDITOR
	//
	// mouse move
	//
	
	onmouseup: function(e) {
		if (!game.renderingInThreeJS && game.selectedSong != -1)
			game.songs[game.selectedSong].onmouseup(e);
	},

	onmousedown: function(e) {
		if (!game.renderingInThreeJS && game.selectedSong != -1)
			game.songs[game.selectedSong].onmousedown(e);
	},

	onmousemove: function(e) {
		if (!game.renderingInThreeJS && game.selectedSong != -1)
			game.songs[game.selectedSong].onmousemove(e);
	},

	
	
	//
	// export / import
	//
	
	saveTextAsFile: function() {
		var thisSong = game.songs[game.selectedSong];
		var textToWrite = thisSong.exportJSON();
		var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
		var fileNameToSaveAs = 'cme_export.json';

		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.innerHTML = "Download File";
		if (window.webkitURL != null)
		{
			// Chrome allows the link to be clicked
			// without actually adding it to the DOM.
			downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
		}
		else
		{
			// Firefox requires the link to be added to the DOM
			// before it can be clicked.
			downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
			downloadLink.onclick = game.destroyClickedElement;
			downloadLink.style.display = "none";
			document.body.appendChild(downloadLink);
		}

		downloadLink.click();
	},
	
	destroyClickedElement: function(event)
	{
		document.body.removeChild(event.target);
	},
	
	loadFileAsText: function()
	{
		var fileToLoad = document.getElementById("fileToLoad").files[0];
		var thisSong = game.songs[game.selectedSong];
		var fileReader = new FileReader();
		fileReader.onload = function(fileLoadedEvent) 
		{
			var textFromFileLoaded = fileLoadedEvent.target.result;
			thisSong.importJSON(textFromFileLoaded);
		};
		fileReader.readAsText(fileToLoad, "UTF-8");
	}
*/	
}

