
//
// init
//

var game;

window.onload = function() {	
	game = new Game();
};



//
// keyboard interaction
//

document.onkeydown = function(e) {

	//if (!event) event = window.event;
    //var code = event.keyCode;
    //if (event.charCode && code == 0) code = event.charCode;
	//console.log(code);
	
	console.log('charcode: ' + e.charCode + ' keycode: ' + e.keyCode);
	if (!game) return;	
		
	switch(game.state) {
		case game.splashScreen:
			game.keybSplash(e);
		break;
		case game.trackSelectionScreen:
			game.keybTrackSelection(e);
			// 122 is F11, enables/disabled fullscreen on firefox
			// 18 is alt. 115 is F4, closes application on windows
			// we need to prevent default behavior to make sure we dont unintentionally scroll down the canvas when selecting the track with the cursor keys
			if ((e.keyCode != 122) && (e.keyCode != 18) && (e.keyCode != 115)) {
				e.preventDefault();
			}
		break;
		case game.instructionsScreen:
			game.keybInstructions(e);
		break;
		case game.countdownScreen:
			game.keybCountdown(e);
		break;
		case game.gameScreen:
			game.keybGame(e);
		break;
		case game.resultScreen:
			game.keybResult(e);
		break;
		case game.rankingScreen:
			game.keybRanking(e);
		break;
	}
	
};


/* EDITOR
//
// mouse interaction
//

window.onmouseup = function(e) {
	if (game) {
		game.onmouseup(e);
	}
}

window.onmousedown = function(e) {
	if (game) {
		game.onmousedown(e);
	}
}

window.onmousemove = function(e) {
	if (game) {
		game.onmousemove(e);
	}
}

function grabScroll(e) {
  if (e.detail !== null) {
    if (e.axis == e.VERTICAL_AXIS) dy = e.detail || e.wheelDeltaY;
  }
  
  var scr = Math.round(normalizeWheelDelta(dy));
  
  if ((game.songs.length > 0) && (game.selectedSong != -1)){
  	var thisSong = game.songs[game.selectedSong];
  	if (thisSong.isPlaying) {
  		thisSong.playpause();
  		thisSong.lastPosition = thisSong.lastPosition - scr*0.005;
  		if (thisSong.lastPosition < 0) thisSong.lastPosition = 0;
  		if (thisSong.lastPosition > thisSong.getDuration()) thisSong.lastPosition = thisSong.getDuration();
  		//thisSong.playpause();
  	} else {
  		thisSong.lastPosition = thisSong.lastPosition - scr*0.005;
  		if (thisSong.lastPosition < 0) thisSong.lastPosition = 0;
  		if (thisSong.lastPosition > thisSong.getDuration()) thisSong.lastPosition = thisSong.getDuration();
  	}

  	thisSong.calcIndexes();
  	
  }
  
  if (dy) { e.preventDefault(); e.stopPropagation(); }
}



//
// window resize
//

window.onresize = function() {
	if (game && this.ctx) {
		game.onwindowresize();
	}
}
*/


rand = function(n){ return Math.floor(Math.random()*n); };


