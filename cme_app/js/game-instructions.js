
Game.prototype.initInstructionsScreen = function() {

	var my = this;
	this.instructionsScreenBG();

	requestAnimFrame( animate );

	var stage = this.stage;
	var renderer = this.renderer;	
	
	// bg
	var bgTexture = this.images[12];
	var bgSprite = new PIXI.Sprite(bgTexture);
	stage.addChild(bgSprite);

	var texture = this.images[13];

	var d = new Date();
	var startTime = d.getTime();
		
	var spotlights = [];
	var nspotlights = 30;
	
	for (var i=0; i<nspotlights; i++) {

		spotlights[i] = new PIXI.Sprite(texture);
		spotlights[i].blendMode = PIXI.blendModes.SCREEN;

		spotlights[i].anchor.x = 0.5;
		spotlights[i].anchor.y = 1.0;

		spotlights[i].position.x = Math.random()*this.cw;
		spotlights[i].position.y = Math.random()*this.ch;
	
		spotlights[i].scale.x = 2.5 + Math.random()*2.5;
		spotlights[i].scale.y = 1.0 + Math.random()*2.5;
	
		spotlights[i].rotation = Math.PI*0.05;
		spotlights[i].thisOpening = 0.001 + Math.random()*0.002;
		spotlights[i].thisSpeed = 0.005 + Math.random()*0.005;
		spotlights[i].alpha = 0.35 + Math.random()*0.25;

		stage.addChild(spotlights[i]);
	}

	function animate() {
		
		// cancel the animation
		if (my.state != my.instructionsScreen) {
			for (var i = stage.children.length - 1; i >= 0; i--) {
				stage.removeChild(stage.children[i]);
			};
			return;
		}
	    
	    // loop the animation
	    requestAnimFrame( animate );

		// apply changes to frame
		var d = new Date();
		var n = startTime - d.getTime();
		for (var i=0; i<spotlights.length; i++) {
	    	spotlights[i].rotation += Math.sin( n * spotlights[i].thisOpening ) * spotlights[i].thisSpeed;
	    }
	    // render
	    renderer.render(stage);
	}
	
	var thisSong = this.songs[this.selectedSong];
	thisSong.lastPosition = 0;
	thisSong.calcIndexes();
	
	game.playSfx(7);
	
};

Game.prototype.instructionsScreenBG = function() {
	
	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs foreground";
		
		var dom_overlay = document.createElement("div");
		dom_overlay.className = "fs overlay_instructions";
		dom.appendChild(dom_overlay);
		
		var dom_drums = document.createElement("div");
		dom_drums.className = "instrument" + this.instrument_visual_order['drums'] + "_instructions";
		dom_drums.style.background = "url('gfx/03_MSMX_instrucoes/instrumentos/drums_bg.png')";
		dom.appendChild(dom_drums);
		for (var i=0; i<4; i++) {
			var layer = document.createElement("div");
			layer.setAttribute("id","drums_bt"+i);
			layer.className = "instrument" + this.instrument_visual_order['drums'] + "_instructions";
			dom.appendChild(layer);
		}
		
		var dom_bass = document.createElement("div");
		dom_bass.className = "instrument" + this.instrument_visual_order['bass'] + "_instructions";
		dom_bass.style.background = "url('gfx/03_MSMX_instrucoes/instrumentos/bass_bg.png')";
		dom.appendChild(dom_bass);
		for (var i=0; i<4; i++) {
			var layer = document.createElement("div");
			layer.setAttribute("id","bass_bt"+i);
			layer.className = "instrument" + this.instrument_visual_order['bass'] + "_instructions";
			dom.appendChild(layer);
		}
		
		var dom_mpc = document.createElement("div");
		dom_mpc.className = "instrument" + this.instrument_visual_order['mpc'] + "_instructions";
		dom_mpc.style.background = "url('gfx/03_MSMX_instrucoes/instrumentos/mpc_bg.png')";
		dom.appendChild(dom_mpc);
		for (var i=0; i<4; i++) {
			var layer = document.createElement("div");
			layer.setAttribute("id","mpc_bt"+i);
			layer.className = "instrument" + this.instrument_visual_order['mpc'] + "_instructions";
			dom.appendChild(layer);
		}
		
		var dom_keys = document.createElement("div");
		dom_keys.className = "instrument" + this.instrument_visual_order['keys'] + "_instructions";
		dom_keys.style.background = "url('gfx/03_MSMX_instrucoes/instrumentos/keys_bg.png')";
		dom.appendChild(dom_keys);
		for (var i=0; i<4; i++) {
			var layer = document.createElement("div");
			layer.setAttribute("id","keys_bt"+i);
			layer.className = "instrument" + this.instrument_visual_order['keys'] + "_instructions";
			dom.appendChild(layer);
		}
		
		// instruments in the bottom left corner		
		var dom_drumsc = document.createElement("div");
		dom_drumsc.className = "instrumentc" + this.instrument_visual_order['drums'] + "_instructions";
		dom_drumsc.style.background = "url('gfx/03_MSMX_instrucoes/03_MSMX_drums_icon.png')";
		dom.appendChild(dom_drumsc);
		
		var dom_bassc = document.createElement("div");
		dom_bassc.className = "instrumentc" + this.instrument_visual_order['bass'] + "_instructions";
		dom_bassc.style.background = "url('gfx/03_MSMX_instrucoes/03_MSMX_bass_icon.png')";
		dom.appendChild(dom_bassc);
		
		var dom_mpcc = document.createElement("div");
		dom_mpcc.className = "instrumentc" + this.instrument_visual_order['mpc'] + "_instructions";
		dom_mpcc.style.background = "url('gfx/03_MSMX_instrucoes/03_MSMX_mpc_icon.png')";
		dom.appendChild(dom_mpcc);
		
		var dom_keysc = document.createElement("div");
		dom_keysc.className = "instrumentc" + this.instrument_visual_order['keys'] + "_instructions";
		dom_keysc.style.background = "url('gfx/03_MSMX_instrucoes/03_MSMX_keys_icon.png')";
		dom.appendChild(dom_keysc);
					
	}
	
};

Game.prototype.interactWithInstructions = function(data) {

	console.log(data);
	var msg = data.split('|');
	var sender = msg[0];
	
	var nameofsender = game.instrument_default_index[sender];
	
	if (msg.length == 5) {
		for (var i=1; i<5; i++) {
			//console.log('this is:' + i);
			var ref = nameofsender + "_bt" + (i-1);
			var layer = document.getElementById(ref);
			
			if (!layer) {
				console.log('no layers?!?!?!');
				return;
			} else {
				if (msg[i] == '1') {
					layer.style.background = "url('gfx/instrumentos/"+ref+".png')";
				} else {
					layer.style.background = "";
				}
			}
		}
	}
	
};

Game.prototype.keybInstructions = function(e) {
	//console.log(e.keyCode);
	switch (e.keyCode) {
		case 13: // return
		console.log('loading countdown...');
			   game.loadState(game.countdownScreen);
		break;
		case 27: // escape
			   game.loadState(game.trackSelectionScreen);
		break;
		case 86: { // v
			this.interactWithInstructions('1|1|0|0|0');
		}
		break;
		case 66: { // b
			this.interactWithInstructions('1|0|1|0|0');
		}
		break;
		case 78: { // n
			this.interactWithInstructions('1|0|0|1|0');
		}
		break;
		case 77: { // m
			this.interactWithInstructions('1|0|0|0|1');
		}
		break;
	}
};
