
Game.prototype.trackSelectionScreenBG = function() {
	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs selectionScreen";
		
		var dom_logo = document.createElement("div");
		dom_logo.className = "fs logo_selection";
		dom.appendChild(dom_logo);
		
		for (var i=0; i<this.songs.length; i++) {
			var title = document.createElement("div");

			title.onmouseover = (function() {
			  var currentI = i;
			  return function() {
			  
					if (game.selectedSnip) {
						game.selectedSnip.stop(0);
						game.selectedSnip = null;
					}
					
					game.selectedSong = currentI;
					
				  	game.changeSelectionArrow(currentI);
				  	
				  	game.selectedSnip = game.playSfx(currentI);
				  
			  }
			})();
			
			title.onclick = (function() {
			  var currentI = i;
			  return function() { 
				  console.log('clicked ' + currentI + ', now loading...');
				  this.selectedSong = currentI;
				  game.loadState(game.instructionsScreen);
			  }
			})();

			title.className = 'selection_' + i;
			title.innerHTML = this.songs[i].songname;
			dom.appendChild(title);
		}

		var dom_arrow = document.createElement("div");
		dom_arrow.setAttribute("id", "selection_arrow");
		dom_arrow.className = "arrow_selection_"+this.selectedSong;
		dom.appendChild(dom_arrow);
	}

	game.playSfx(6);

	game.stopBackgroundMusic();

};

Game.prototype.changeSelectionArrow = function(id) {
	var dom_arrow = document.getElementById('selection_arrow');
	if (dom_arrow) {
		dom_arrow.className = "arrow_selection_"+id;
	}
};

Game.prototype.keybTrackSelection = function(e) {
	switch (e.keyCode) {
		case 13: // return
			    game.loadState(game.instructionsScreen);
		break;
		case 27: // escape
			    game.loadState(game.splashScreen);
		break;
		case 38: {
				if (game.selectedSnip) {
					game.selectedSnip.stop(0);
					game.selectedSnip = null;
				}
				
				game.selectedSong--;
				if (game.selectedSong < 0) game.selectedSong = game.songs.length-1;
				game.changeSelectionArrow(game.selectedSong);
				
				game.selectedSnip = game.playSfx(game.selectedSong);
		}
		break;
		case 40: {
				if (game.selectedSnip) {
					game.selectedSnip.stop(0);
					game.selectedSnip = null;
				}
				
			    game.selectedSong++;
			    if (game.selectedSong >= game.songs.length) game.selectedSong = 0;
			    game.changeSelectionArrow(game.selectedSong);
			   
			    game.selectedSnip = game.playSfx(game.selectedSong);
		}
		break;
	}
};

