
Game.prototype.initResultScreen = function() {

	// if still playing, stop
	var thisSong = this.songs[this.selectedSong];
	if (thisSong.isPlaying) thisSong.playpause();
	
	// stop drawing
	this.isDrawing = false;

/* EDITOR	
	// remove triggerline
	var dom_tl = document.getElementById('triggerline');
	if (dom_tl) document.body.removeChild(dom_tl);
*/
	this.resultScreenBG();
	
	game.playSfx(3);
	
};

Game.prototype.resultScreenBG = function() {
	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs resultScreen";

		// logo
		var dom_logo = document.createElement("div");
		dom_logo.className = "fs logo_result";
		dom.appendChild(dom_logo);
		
		// input box
		var dom_entername = document.createElement("div");
		dom_entername.className = "entername_result";
		dom_entername.innerHTML = 'NOME DA EQUIPA:';
		dom.appendChild(dom_entername);
		
		var dom_underline = document.createElement("div");
		dom_underline.className = "underline_result";
		dom_underline.innerHTML = '______________';
		dom.appendChild(dom_underline);
		
		var dom_inputbox = document.createElement("input");
		dom_inputbox.setAttribute("id", "inputbox");
		dom_inputbox.setAttribute("autofocus", "autofocus");
		dom.appendChild(dom_inputbox);
		// firefox doesnt seem to follow autofocus, you need to call it after the dom is updated
		setTimeout(function(){
			dom_inputbox.focus();
		}, 500);
		
		// top text
		var adjectives = ['Brutal','Excelente','Altamente','Espectacular','Boa','Bestial'];
		var dom_textresult = document.createElement("div");
		dom_textresult.className = "text_result";
		dom_textresult.innerHTML = adjectives[Math.floor(Math.random()*adjectives.length)]+'!!!';
		dom.appendChild(dom_textresult);
		
		// face with animation
		var dom_imageresult = document.createElement("div");
		dom_imageresult.className = "image_result";
		dom.appendChild(dom_imageresult);
		
		var dom_imageresultoverlay = document.createElement("div");
		dom_imageresultoverlay.className = "image_overlay_result";
		dom.appendChild(dom_imageresultoverlay);
		
		var newName = 'bounce';
		var maxHeight = 504; 
		var newHeight = parseInt(maxHeight * game.scorePercentage, 10);
		/* animation not working in firefox
		var lastSheet = document.styleSheets[document.styleSheets.length - 1];
lastSheet.insertRule("@-moz-keyframes " + newName + " { from { clip: rect(504px, 504px, 504px, 0px); } to { clip: rect("+ (504 - newHeight) +"px, 504px, 504px, 0px);} }", lastSheet.cssRules.length);
		dom_imageresultoverlay.style.clip = 'rect(' + (maxHeight - newHeight) + 'px, 504px, 504px, 0px)';
		dom_imageresultoverlay.style.mozAnimationName = 'bounce';
		dom_imageresultoverlay.style.mozAnimationDuration = '4s';*/
		dom_imageresultoverlay.style.clip = 'rect(' + (maxHeight - newHeight) + 'px, 504px, 504px, 0px)';
		
		// score multipliers
		var padY = 9;
		var posY = 28;
		var padX = 4.8;
		var posX = 72;
		
		for (var i = 0; i < game.numspecials; i++) {
			var dom_scm = document.createElement("div");
			if (game.scoreMultiplier < i+2) {
				dom_scm.className = 'scm_result';
			} else {
				dom_scm.className = 'scmlit_result';
			}
			var thisx = posX + (i%4) * padX;
			var thisy = posY + Math.floor(i/4) * padY;
			dom_scm.style.left = thisx + '%';
			dom_scm.style.top = thisy + '%';
			
			dom.appendChild(dom_scm);
		}

		var dom_valueresult = document.createElement("div");
		dom_valueresult.className = "value_result";
		dom_valueresult.innerHTML = parseInt(game.scorePercentage*100, 10) + '%';
		dom.appendChild(dom_valueresult);
	}
};

Game.prototype.keybResult = function(e) {
	switch (e.keyCode) {
		case 13: // return
			game.loadState(game.rankingScreen);
		break;
	}
};
