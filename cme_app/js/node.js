
function TrackNode(yPos, parent) {
	this.yPos = yPos;
	this.parent = parent;
	this.images = parent.images;
	this.imageID = parent.imageID;

	this.nodeID = uid.get_uid();
	
	this.stage = game.stage;
	this.sprite = null;
	
	this.ch = game.ch;

	this.radius = 20;
	this.isLit = false;
	this.isSpecial = false;
}

TrackNode.prototype = {

	updateYPos: function(newPos) {
		this.yPos = newPos;
	},
	
	initStage: function() {
		this.sprite = new PIXI.Sprite(this.images[this.imageID]);

		this.sprite.anchor.x = 0.5;
		this.sprite.anchor.y = 0.5;

		this.sprite.position.x = -100;
		this.sprite.position.y = -100;

		this.stage.addChild(this.sprite);
		
		this.isLit = false;
	},
	
	animateStage: function(xPos, yRef) {
		if (!this.sprite) return;
	
		var thisy = yRef - this.yPos;
	
		if ((thisy > -this.radius) && (thisy < 1080+this.radius)) {
			
			var alpha = 1.0;
			var fadeout_threshold1 = 800;
			var fadeout_threshold2 = 930;
			if (thisy > fadeout_threshold2 + 40) return;
			if (thisy > fadeout_threshold1) {
				alpha = 1.0 - ((thisy - fadeout_threshold1) / (fadeout_threshold2 - fadeout_threshold1));
			}

			var size = 0.4*(1.35-(this.ch-thisy)/this.ch);
	
			var imgref = this.imageID;
	
			if (this.isSpecial) {
				imgref = 0;
			}
	
			if (!this.isLit) {
				this.sprite.alpha = alpha;
				this.sprite.position.x = xPos;
				this.sprite.position.y = thisy;
				this.sprite.scale.x = size;
				this.sprite.scale.y = size;
				this.sprite.setTexture(this.images[imgref]);
				
				//console.log(xPos + ' ' + thisy + ' ' + imgref + ' ' + this.images[imgref]);
			} else {
				this.sprite.alpha = alpha*2.5;
				this.sprite.position.x = xPos;
				this.sprite.position.y = thisy;
				this.sprite.scale.x = size*1.1;
				this.sprite.scale.y = size*1.1;
				this.sprite.setTexture(this.images[imgref+1]);
			}
			
		}
	},
	
	hideSprite: function() {
		if (this.sprite) {
			this.sprite.position.x = -100;
			this.sprite.position.y = -100;
			this.sprite.alpha = 0;
		}
	},
	
	
	
	//
	// mouse handling
	//
	
	onmousedown: function(e) {
		var hit = false;
	
		if (Math.abs(this.ch - e.pageY + e.ref - this.yPos - this.parent.bottomPad) < this.radius*2) hit = true;
		
		if (hit) {
			this.parent.selectedNode = this.nodeID;
				
			if(e.shiftKey) {
			  this.parent.deleteSelectedNode();
			}
		}
	},

	onmousemove: function(e) {
		this.updateYPos(this.ch - e.pageY + e.ref - this.parent.bottomPad);		
	}
	
}
