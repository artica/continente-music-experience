
Game.prototype.initSplashScreen = function() {

	var my = this;
	
	this.splashScreenBG();

	requestAnimFrame( animate );
	
	var stage = this.stage;
	var renderer = this.renderer;
	
	// bg
	var bgTexture = this.images[10];
	var bgSprite = new PIXI.Sprite(bgTexture);
	stage.addChild(bgSprite);

	var texture = this.images[11];

	var d = new Date();
	var startTime = d.getTime();
		
	var spotlights = [];
	var nspotlights = 25;
	
	for (var i=0; i<nspotlights; i++) {

		spotlights[i] = new PIXI.Sprite(texture);
		spotlights[i].blendMode = PIXI.blendModes.SCREEN;

		spotlights[i].anchor.x = 0.5;
		spotlights[i].anchor.y = 0.0;

		spotlights[i].position.x = Math.random()*this.cw;
		spotlights[i].position.y = Math.random()*this.ch*0.1 - 200;
	
		spotlights[i].scale.x = 1.0 + Math.random()*1.5;
		spotlights[i].scale.y = 1.5 + Math.random()*2.5;
	
		spotlights[i].rotation = Math.PI*0.05;
		spotlights[i].thisOpening = 0.001 + Math.random()*0.002;
		spotlights[i].thisSpeed = 0.005 + Math.random()*0.005;
		spotlights[i].alpha = 0.25 + Math.random()*0.25;

		stage.addChild(spotlights[i]);
	}
/*	
	var posX = 0;
	var prevX = 0;
	var posY = 0;
	var step = 30;
	var maxsteps = 50;
	
	var shiftY = 800;
	var flexY = -0.0008;
	var shiftX = -700;
	
	for (var i = 0; i < maxsteps; i++) {
	
		var graphics = new PIXI.Graphics();
		graphics.beginFill(0xCCCCCC);

	
		posX = i*step;
		prevY = shiftY + flexY*(prevX + shiftX)*(prevX + shiftX);
		posY = shiftY + flexY*(posX + shiftX)*(posX + shiftX);
		console.log(prevX + ' ' + prevY + ' ' + posX + ' ' + posY);
		
		var thick = step * (1.0 - (i/maxsteps)) * 1.5;

		
		graphics.moveTo(prevX, prevY);
		graphics.lineTo(prevX, prevY+thick);
		graphics.lineTo(posX, posY+thick);
		graphics.lineTo(posX, posY);
		
		graphics.endFill();
		graphics.blendMode = PIXI.blendModes.NORMAL;
		graphics.alpha = 1.0 - (i/maxsteps);

		stage.addChild(graphics);


		prevX = posX;
		
	}
*/
	function animate() {
		
		// cancel the animation
		if (my.state != my.splashScreen) {
			for (var i = stage.children.length - 1; i >= 0; i--) {
				stage.removeChild(stage.children[i]);
			};
			return;
		}
	    
	    // loop the animation
	    requestAnimFrame( animate );

		// apply changes to frame
		var d = new Date();
		var n = startTime - d.getTime();
		for (var i=0; i<spotlights.length; i++) {
	    	spotlights[i].rotation += Math.sin( n * spotlights[i].thisOpening ) * spotlights[i].thisSpeed;
	    }
	    // render
	    renderer.render(stage);
	}

	game.testBackgroundMusic();

};

Game.prototype.splashScreenBG = function() {
	var dom = document.getElementById("bgcontainer")
	if (dom) {
		dom.innerHTML = '';
		dom.className = "fs foreground";
		
		var dom_logo = document.createElement("div");
		dom_logo.className = "fs logo_splash";
		dom.appendChild(dom_logo);
		
		var dom_overlay = document.createElement("div");
		dom_overlay.className = "fs overlay_splash";
		dom.appendChild(dom_overlay);

		var dom_arrow = document.createElement("div");
		dom_arrow.className = "arrow_splash";
		dom.appendChild(dom_arrow);
		
		dom_arrow.onclick = function() {
			game.loadState(game.trackSelectionScreen);
		}
	}
	
	game.playSfx(7);
};

Game.prototype.keybSplash = function(e) {
	switch (e.keyCode) {
		case 13: // return
			game.loadState(game.trackSelectionScreen);
		break;
		case 27: // escape
			game.loadState(game.rankingScreen);
		break;
	}
};
