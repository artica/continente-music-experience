
function UID(last_uid) {
	this.last_uid = last_uid ? last_uid : 0;
}

UID.prototype = {
	get_uid: function() {
		this.last_uid++;
		return this.last_uid.toString(36);
	}
}

var uid = new UID();

//console.log('this uid exists');
