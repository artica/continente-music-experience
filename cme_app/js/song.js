
function Song(songid, songname, syncfreq, parent) {

	this.songID = songid;
	this.songname = songname;

	this.pixelsPerSecond = 1000*0.25;
    this.ac = this.getAudioContext();
    this.isPlaying = false;
    this.lastPosition = 0.0;
    this.lastUpdate = 0.0;
    this.updateRate = 0.2; // time in seconds where we update to check if a note has been missed
    this.startTime = 0.0;
    this.offlineAc = this.getOfflineAudioContext(this.ac.sampleRate);
    this.trackDuration = 0.0;
    
    this.samples = 128;
    this.fft = null;
    this.syncfreq = syncfreq;
        
    this.peaks = [];
    this.instruments = [];
    this.background;
    
    this.tracks = [];
    this.numnodes = 0;
    
    this.bufferList = [];
    
    this.images = parent.images;

}

Song.prototype = {

	load: function(cb) {
		var my = this;
		
		//
		// load all channels
		//
		
		this.bufferLoader = new BufferLoader(
			this.offlineAc,
			[
			  'songs/'+this.songID+'/original.wav',
			  'songs/'+this.songID+'/instrument1.wav',
			  'songs/'+this.songID+'/instrument2.wav',
			  'songs/'+this.songID+'/instrument3.wav',
			  'songs/'+this.songID+'/instrument4.wav'
			],
			function finishedLoading(bufferList) {
				//console.log('loaded all items in bufferList ' + bufferList);
				my.bufferList = bufferList;

				my.getPeaks();
				my.initInstruments();		
				my.initTracks();
				my.background = new Background(my.ac);

				$.getJSON( "songs/" + my.songID + "/nodes.json", function( temp ) {

					my.numnodes = 0;

					// import nodes
					for (var i=0; i<temp.length; i++) {
											
						var idx = game.instrument_visual_order[game.instrument_default_index[Math.floor(i/4)]]*4 + i%4;					
					
						console.log('loading ' + i + ' into ' + idx);
						my.tracks[idx].nodes = [];

						for (var j=0; j<temp[i].length; j++) {
							//console.log('import this node: ' + i + ' ' + temp[i][j]);
							my.tracks[idx].addNode(temp[i][j]);
							my.numnodes++;
						}

					}
					
					// generate the special nodes
					if (my.numnodes < game.numspecials) {
						alert('not enough nodes to create the specials');
						return;
					}
					var specials = game.numspecials;
					while(specials > 0) {
						var track = my.tracks[rand(my.tracks.length)];
						//var track = my.tracks[13];
						var node = track.nodes[rand(track.nodes.length)];
						if (!node.isSpecial) {
							node.isSpecial = true;
							specials--;
						}
					}
					
					// calculate max score (need it for game score percentage)
					my.maxscore = my.numnodes * game.pickwindows.length * game.pickmultiplier * (game.numspecials * 2);
					
					cb();
				});
				
				my.trackDuration = my.getDuration();
			  
			}
		);
		this.bufferLoader.load();
		
	},

	initInstruments: function() {

		//var idx = getKeyByValue(0)];
		//console.log('rev index of 0: ' + game.instrument_default[getKeyByValue(0)]);
	
		var base = 23; // ref of image resources
		var idx0 = base + game.getKeyByValue(0)*2;					
		var idx1 = base + game.getKeyByValue(1)*2;					
		var idx2 = base + game.getKeyByValue(2)*2;					
		var idx3 = base + game.getKeyByValue(3)*2;
		
		console.log('initializing instruments');
		this.instruments = [
			new Instrument(this.ac, 0.3, 	idx0),
			new Instrument(this.ac, 0.48, 	idx1),
			new Instrument(this.ac, 0.66, 	idx2),
			new Instrument(this.ac, 0.845, 	idx3)
			];
	},

	initTracks: function() {
		console.log('initializing tracks');

		var idx0 = 0;//game.instrument_default[game.getKeyByValue(0)];					
		var idx1 = 1;//game.instrument_default[game.getKeyByValue(1)];					
		var idx2 = 2;//game.instrument_default[game.getKeyByValue(2)];					
		var idx3 = 3;//game.instrument_default[game.getKeyByValue(3)];					
					
		this.tracks = [
		
			new Track( 0.3, 	2, this, this.instruments[idx0]),
			new Track( 0.3285,  4, this, this.instruments[idx0]),
			new Track( 0.357, 	6, this, this.instruments[idx0]),
			new Track( 0.385,  	8, this, this.instruments[idx0]),
			
			new Track( 0.4815,  2, this, this.instruments[idx1]),
			new Track( 0.51, 	4, this, this.instruments[idx1]),
			new Track( 0.5385,  6, this, this.instruments[idx1]),
			new Track( 0.5675, 	8, this, this.instruments[idx1]),
			
			new Track( 0.6635,	2, this, this.instruments[idx2]),
			new Track( 0.6915,	4, this, this.instruments[idx2]),
			new Track( 0.72,  	6, this, this.instruments[idx2]),
			new Track( 0.7485,	8, this, this.instruments[idx2]),
			
			new Track( 0.845, 	2, this, this.instruments[idx3]),
			new Track( 0.8748, 	4, this, this.instruments[idx3]),
			new Track( 0.9025,	6, this, this.instruments[idx3]),
			new Track( 0.9315,	8, this, this.instruments[idx3])
		];
	},

	getAudioContext: function () {
		return new (
				window.AudioContext || window.webkitAudioContext
			);
	},

	getOfflineAudioContext: function (sampleRate) {
		return new (
				window.OfflineAudioContext || window.webkitOfflineAudioContext
			)(1, 2, sampleRate);
	},
	
	updatePosition: function() {
		if (this.isPlaying) {

			this.lastPosition = this.getEllapsedTime();
			
			//console.log('checking:' + this.lastPosition + ' ' + this.trackDuration); 
			//if (this.lastPosition > 4.0) { // to test transitions
			if (this.lastPosition > this.trackDuration) {
				game.loadState(game.resultScreen);
				return;
			}

			if (this.lastUpdate + this.updateRate < this.lastPosition) {
		
				// gradually recover volume of all instruments
				for (var i=0; i < this.instruments.length; i++) {
					this.instruments[i].increaseGain(0.02, null);
				}
			
				this.lastUpdate = this.lastPosition;
			}
			
		} else {
			this.lastUpdate = this.lastPosition;
		}
		
		//console.log('calc last pos: ' + this.lastPosition);
		var ref = this.getLastPositionPixels();
		return ref;
	},
	
	initStage: function() {
		for (var i=0; i<this.instruments.length; i++) {
			this.instruments[i].initStage();
		}
		for (var i=0; i<this.tracks.length; i++) {
			this.tracks[i].initStage(19 + i%4);
		}
	},
	
	animateStage: function() {
		var ref = this.updatePosition();
		if (!ref) return;
		
		var d = new Date();
		var time = d.getTime();
		
		for (var i=0; i<this.instruments.length; i++) {
			this.instruments[i].animateStage(time);
		}
		for (var i=0; i<this.tracks.length; i++) {
			this.tracks[i].animateStage(ref, time);
		}
	},
	
	getPeaks: function () {
		console.log('get peaks');
	
		// get length
		var length = Math.ceil( this.getDuration() * this.pixelsPerSecond);	
		var buffer = this.bufferList[0];
		
		// calculate peaks
		var sampleSize = Math.ceil(buffer.length / length);
		var sampleStep = ~~(sampleSize / 10);
		var channels = buffer.numberOfChannels;
		var peaks = new Float32Array(length);
		for (var c = 0; c < channels; c++) {
			var chan = buffer.getChannelData(c);
			//if (c == 0) console.log(chan[0]);
			for (var i = 0; i < length; i++) {
				var start = ~~(i * sampleSize);
				var end = start + sampleSize;
				var peak = 0;
				for (var j = start; j < end; j += sampleStep) {
					var value = chan[j];
					if (value > peak) {
						peak = value;
					} else if (-value > peak) {
						peak = -value;
					}
				}
				if (c > 1) {
					peaks[i] += peak / channels;
				} else {
					peaks[i] = peak / channels;
				}
			}
		}
		
		this.peaks = peaks;		
	},

	getDuration: function() {
		return this.bufferList[0] ? this.bufferList[0].duration : 0;
	},

	getDurationPixels: function() {
		return Math.ceil( this.getDuration() * this.pixelsPerSecond );
	},

	getEllapsedTime: function() {
		var ellapsedTime = this.ac.currentTime - this.startTime;
		
		var dom = document.getElementById('time');
		if (dom) {
			dom.innerHTML = ellapsedTime.toFixed(3);
		}
	
		return ellapsedTime;
	},

	getEllapsedPixels: function() {
		return Math.ceil(this.getEllapsedTime() * this.pixelsPerSecond);
	},
	
	getLastPositionPixels: function() {
		return Math.ceil(this.lastPosition * this.pixelsPerSecond);
	},

	playpause: function() {
		if ((this.bufferList[0]) && (this.ac)) {
			if (this.isPlaying) {
				this.lastPosition = this.getEllapsedTime();
				//console.log('last position when stop: ' + this.lastPosition);
				for (var i = 0; i < this.instruments.length; i++) {
					this.instruments[i].stop();
			  	}
			  	this.background.stop();
				this.isPlaying = false;
			} else {
				//console.log('press play on tape ' + this.instruments.length + ' instruments starting from ' + this.lastPosition);
				
				var i = 0;
				
				for (i=0; i<this.instruments.length; i++) {
					var idx = game.instrument_visual_order[game.instrument_default_index[i]];					
					this.instruments[idx].create( this.bufferList[ i + 1 ] );
				}
				
				this.background.create(this.bufferList[0], 0.1);
				
				for (var i=0; i<this.instruments.length; i++) {
					this.instruments[i].play(this.lastPosition);
				}
				this.background.play(this.lastPosition);
				
				//this.instruments[0].play(this.lastPosition);
				this.startTime = this.ac.currentTime - this.lastPosition;
			  	this.isPlaying = true;
			}
		}
	},
	
	calcIndexes: function() {
		for (var i=0; i<this.tracks.length; i++) {
			this.tracks[i].calcIndexes();
		}
	},


/* EDITOR
	//
	//	mouse handling
	//

	onmouseup: function(e) {
		//console.log('mouseup on song with ' + e.pageX + ' ' + e.pageY);

		// clean all selected nodes
		for (var i=0; i<this.tracks.length; i++) {
			this.tracks[i].selectedNode = -1;
		}
	},

	onmousedown: function(e) {
		//console.log('mousedown on song with ' + e.pageX + ' ' + e.pageY);

		// if hitting nothing insert node
		// if hitting a node and pressing shift, delete that node
		e.ref = this.getLastPositionPixels();
		for (var i=0; i<this.tracks.length; i++) {
			this.tracks[i].onmousedown(e);
		}
	},

	onmousemove: function(e) {
		//console.log('mousemove on song with ' + e.pageX + ' ' + e.pageY);
		
		// if it matches selectedNode update position
		e.ref = this.getLastPositionPixels();
		for (var i=0; i<this.tracks.length; i++) {
			this.tracks[i].onmousemove(e);
		}
	},


	
	//
	// export / import
	//
	
	exportJSON: function() {
		var output = [];
		for (var i=0; i<this.tracks.length; i++) {
			output.push( this.tracks[i].exportJSON() );
		}
		
		this.jsonstring = JSON.stringify(output);
		
		return this.jsonstring;
	},
	importJSON: function(jsonstring) {
		// position line marker on starting position
		window.scrollTo(0, document.body.scrollHeight);
		this.lastPosition = window.pageYOffset;
		
		// parse json
		var temp = JSON.parse(jsonstring);
		console.log(temp);
		
		if ((temp) && (temp.length != this.tracks.length)) {
			alert('import doesnt match number of tracks');
		} else {
			var dom = document.getElementById('nodebay');
			if (dom) {
				dom.innerHTML = '';
			}
			
			// import nodes
			for (var i=0; i<temp.length; i++) {
				this.tracks[i].nodes = [];
				
				for (var j=0; j<temp[i].length; j++) {
					console.log('import this node: ' + i + ' ' + temp[i][j]);
					this.tracks[i].addNode(temp[i][j]);	
				}
				
			}
		}
		
	}
*/
}
